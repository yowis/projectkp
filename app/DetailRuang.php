<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailRuang extends Model
{
   protected $connection = 'pgsql';
   protected $table = 'dtrans_ruang';

   public $fillable = [
      'idtrans', 'idlokasi', 'idunit', 'tanggalmulai', 'tanggalselesai','jammulai','jamselesai',
   ];

   public $timestamps = false;

   // public function detai()
   // {
   //    return $this->hasOne('App\EventCalendar', 'idtrans', 'idtrans');
   // }
   public function eventcalendar()
   {
      return $this->hasOne('App\EventCalendar', 'id', 'idtrans');
   }

   public function room()
   {
      return $this->hasOne('App\Room', 'idlokasi', 'idlokasi');
   }
}
