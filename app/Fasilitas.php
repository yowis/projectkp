<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Fasilitas;

class Fasilitas extends Model
{
    //
   protected $connection = 'pgsql_uwminv';
   protected $table = 'in_barang';
   protected $primaryKey = 'idbarang';
   protected $keyType = 'string';
   public $timestamps = false;

   public $fillable = [
      'idbarang', 'namabarang', 'tipebarang', 'idsatuan',
   ];


}
