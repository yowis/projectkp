<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
   protected $table = "sc_user";
   protected $connection = "pgsql_uwmsdm";
   protected $primaryKey = 'userid';
   public $timestamps = false;

   public $fillable = [
      'userid', 'password', 'hints', 'builtin', 'expireddate', 't_logintime', 't_ipaddress',
       't_hostname', 'nama', 'statususer', 'passwd',
   ];

   public function userrole()
   {
      return $this->hasMany('App\UserRole', 'userid', 'userid');
   }

}
