<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Room;
use App\Gedung;
use App\TipeLokasi;
use App\Unit;

class RuangController extends Controller
{
    //
   public function list_Ruang()
   {
      $data = Room::with('gedung')->get();
      // dd($data);
      return view('backend.list_ruang',[ 'Room' => $data ]);
   }

   public function input_ruang()
   {
      $gedung     = Gedung::all();
      $tipelokasi = TipeLokasi::all();
      $unit       = Unit::all();
      return view('backend.input_ruang', [
         'gedung'       => $gedung,
         'tipelokasi'   => $tipelokasi,
         'unit'         => $unit,
      ]);
   }

   public function add_ruang(Request $request)
   {

      $rules = [
         'idunit'          => 'required',
         'idlokasi'        => 'required|unique:in_lokasi,idlokasi',
         'idgedung'        => 'required',
         'idtipelokasi'    => 'required',
         'namaruang'      => 'required',
      ];

      $customMessages = [
        'required'   => ':attribute field can not be blank.',
        'unique'     => ':attribute tidak boleh sama',
      ];

      $this->validate($request, $rules, $customMessages);

      $ruang                  = new Room;
      $ruang->idunit          = $request->idunit;
      $ruang->idlokasi        = $request->idlokasi;
      $ruang->idgedung        = $request->idgedung;
      $ruang->idtipelokasi    = $request->idtipelokasi;
      $ruang->namalokasi      = $request->namaruang;
      $ruang->panjang         = $request->panjang;
      $ruang->lebar           = $request->lebar;
      $ruang->tinggi          = $request->tinggi;
      $ruang->kapasitas       = $request->kapasitas;
      $ruang->luas            = $request->luas;
      $ruang->kondisi         = $request->kondisi;
      $ruang->fungsiruang     = $request->fungsiruang;
      $ruang->lantai          = $request->lantai;
      $ruang->interkom        = $request->interkom;
      $ruang->jenjang         = $request->jenjang;

      $ruang->save();
      $request->session()->flash('success', 'Insert Ruang berhasil');
      return redirect()->route('list_ruang');

   }

   public function edit_ruang(Request $request)
   {

      $id         = $request->id;
      $all_data   = Room::find($id);
      $gedung     = Gedung::all();
      $tipelokasi = TipeLokasi::all();
      $unit       = Unit::all();
      return view('backend.edit_ruang',[
         'ruang' => $all_data ,
         'id' => $id ,
         'gedung' => $gedung ,
         'tipelokasi' => $tipelokasi ,
         'unit' => $unit
      ]);
   }

   public function update_ruang(Request $request)
   {
      $id = $request->idlokasi;
      // dd($id);
      $ruang = Room::find($id);
      $ruang->idunit          = $request->idunit;
      $ruang->idlokasi        = $request->idlokasi;
      $ruang->idgedung        = $request->idgedung;
      $ruang->idtipelokasi    = $request->idtipelokasi;
      $ruang->namalokasi      = $request->namaruang;
      $ruang->panjang         = $request->panjang;
      $ruang->lebar           = $request->lebar;
      $ruang->tinggi          = $request->tinggi;
      $ruang->kapasitas       = $request->kapasitas;
      $ruang->luas            = $request->luas;
      $ruang->kondisi         = $request->kondisi;
      $ruang->fungsiruang     = $request->fungsiruang;
      $ruang->lantai          = $request->lantai;
      $ruang->interkom        = $request->interkom;
      $ruang->jenjang         = $request->jenjang;

      $ruang->save();
      $request->session()->flash('success', 'Ruang berhasil di update');
      return redirect()->route('list_ruang');
   }
}
