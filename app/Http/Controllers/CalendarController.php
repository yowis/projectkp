<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\LoginController;
use App\DetailRuang;
use Validator;
use Calendar;

class CalendarController extends Controller
{
   public function __construct()
   {

   }

   public function index2()
   {

   }

   //Load data ke dalam plugin calendar
   public function index()
   {
      $calendar = DetailRuang::all();
      $event = [];

      //arrray event CALENDAR pada database
      foreach($calendar as $key => $value){
         $datetime_mulai = $value->tanggalmulai . " " . $value->jammulai;
         $datetime_selesai = $value->tanggalselesai . " " . $value->jamselesai;
         $event[] = Calendar::event(
            $value->eventcalendar->namakegiatan,
            false,
            new \DateTime($datetime_mulai),
            new \DateTime($datetime_selesai),
            $value->id,
            [
         		'url' => 'calendar/'.$value->id
         	]

         );
      }

      $calendar_details = Calendar::addEvents($event);

      return view('backend.calendar',compact('calendar_details'));

   }

   public function showDashboard(){
      return view('backend.dashboard');
   }
}
