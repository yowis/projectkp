<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Room;
use App\Fasilitas;
use App\Transaksi;
use App\EventCalendar;
use App\DetailRuang;
use App\Detailbarang;
use File;

class TransaksiController extends Controller
{

   public function approve_transaksi($id){
      $find_data = EventCalendar::find($id);
      // $find_data->persetujuan_wr = 'SETUJU';
      // $find_data->persetujuan_baak = 'SETUJU';
      $find_data->persetujuan_bau = 'SETUJU';
      $find_data->save();

      $alldata = EventCalendar::all();
      return view('backend.list_transaksi' , ['transaksi' => $alldata ]);
   }
   public function list_transaksi()
   {
      $data_transaksi = EventCalendar::all();
      return view('backend.list_transaksi', ['transaksi' => $data_transaksi] );
   }

   public function Detail_transaksi($id){
      $data_transaksi = EventCalendar::find($id);
      return view('backend.detail_transaksi',['data_transaksi' => $data_transaksi]);
   }

   public function edit_transaksi($id){
      $alldata = EventCalendar::find($id);
      // $detail_trans = DetailRuang::where('idtrans','=',$alldata->id)->get();
      // $last_date = array();
      // foreach ($detail_trans as $key => $value) {
      //    $daterange_query = DB::select("
      //                                     Select MAX(tanggalmulai) as tanggalselesai,count(idlokasi) from dtrans_ruang Where idtrans = :idtrans
      //                                     and idlokasi = :idlokasi
      //                                  ",
      //                                  [
      //                                     'idtrans' => $alldata->id,
      //                                     'idlokasi'=> $value->idlokasi,
      //                                  ]);
      //    $last_date[] = $daterange_query;
      // }

      $room = Room::all();
      $fasilitas = Fasilitas::all();
      $count_detailtrans = DetailRuang::where('idtrans', $alldata->id)->count();

      return view('backend.edit_transaksi' , [
         'datatrans' => $alldata,
         'id'  => $id,
         'rooms' => $room,
         'fasilitas' => $fasilitas,
         'rowIdRuang' => $count_detailtrans
      ]);
   }

   public function update_transaksi_ruang(Request $request)
   {

      $this->validate($request, [
          'namakegiatan' => 'required|max:255',
          'tanggalpengajuan' => 'required',
          'jumlahpeserta' => 'required|numeric',
          'bentukkegiatan' => 'required|max:255',
          'lampiran' => 'mimes:pdf',
          'namapenanggungjawab' => 'required|max:255',
          'tlppenanggungjawab' => 'required|numeric',
      ]);
      $filename = null;
      $find_data = EventCalendar::find($request->id);
      $old_file = $find_data->lampiran;
      $find_data->namakegiatan = $request->namakegiatan;
      $find_data->tanggalpengajuan = $request->tanggalpengajuan;
      $find_data->jumlahpeserta = $request->jumlahpeserta;
      $find_data->keterangankegiatan = $request->keterangan;
      $find_data->bentukacara = $request->bentukkegiatan;
      $find_data->penanggungjawab = $request->namapenanggungjawab;
      $find_data->notelp_penanggungjawab = $request->tlppenanggungjawab;
      $find_data->emailpenanggungjawab = $request->emailpenanggungjawab;

      $temptglMulai = $request->tanggalmulai;
      $temptglSelesai = $request->tanggalselesai;
      $tempJamMulai = $request->jammulai;
      $tempJamSelesai = $request->jamselesai;
      $listRuang = $request->data_ruang;
      $listBarang = $request->data_fasilitas;
      $jumlahBarang = $request->jumlahbarang;

      // $temptglMulai = $request->tanggalmulai;
      // $temptglSelesai = $request->tanggalselesai;
      // $listRuang = $request->data_ruang;
      // $listBarang = $request->data_fasilitas;
      // $jumlahBarang = $request->jumlahbarang;
      $id_dtransRuang = $request->id_dtransRuang;
      $id_dtransFasilitas = $request->id_dtransFasilitas;

      if ($request->file('lampiran')){
        $filename = $request->lampiran->getClientOriginalName();
        $request->lampiran->move('upload/lampiran/', $filename);
        if (File::exists($old_file)){
          File::delete($old_file);
        }
        $find_data->lampirankegiatan = $filename;
      }

      $find_data->save();
      $id = $find_data->id;

      if($listRuang != null){
         for ($i=0; $i < count($listRuang); $i++) {
            $temp = explode("$", $listRuang[$i]);
            // $potong_waktu_mulai   = explode(" ",$temptglMulai[$i]);
            // $potong_waktu_selesai = explode(" ",$temptglSelesai[$i]);
            $idlokasi = $temp[0];
            // $idunit = $temp[1];

            // $datemulai_not_equals = DB::table('dtrans_ruang')
            //                         ->where('idlokasi', $idlokasi)
            //                         ->where('tanggalmulai' , $start_date)->get();
            // if(count($datemulai_not_equals) == 0){
               // $daterang_query = DB::select("select * from dtrans_ruang where idlokasi=".$idlokasi." AND ((tanggalmulai <=".$start_date." AND tanggalmulai >=".$end_date.")OR(tanggalselesai >=".$start_date." AND tanggalselesai <=".$end_date."))");
               // dd($idlokasi);

               $daterange_query = DB::select("
                                                select * from dtrans_ruang
                                                where idlokasi = :idlokasi AND substring(tanggalmulai::text,1,10)::date = to_date(:tanggalmulai,'YYYY-MM-DD')
                                                AND (jammulai between :jammulai and :jamselesai OR jamselesai between :jammulai and :jamselesai)
                                             ",
                                             [
                                                'tanggalmulai' => $temptglMulai[$i],
                                                'idlokasi' => $idlokasi,
                                                'jammulai'=> $tempJamMulai[$i],
                                                'jamselesai'   => $tempJamSelesai[$i]
                                             ]);

               // dd(count($daterange_query));
               // dd(count($daterange_query));
                  // foreach ($daterange as $key => $value) {
               if(count($daterange_query) > 0){
                  $request->session()->flash("errorsBooking", 'Ruang sedang di Gunakan pada tanggal dan jam Tersebut');
                  return redirect()->route('edit_transaksi', ['id' => $request->id]);
               }else{
                  for ($j=0; $j < count($temptglMulai) ; $j++) {
                     $diff_date = date_diff(date_create($temptglMulai[$j]),date_create($temptglSelesai[$j]));
                     $date_diff_counter = $temptglMulai[$j];
                     for ($i = 0 ; $i <= $diff_date->d; $i++){
                        $dtransRuang = new DetailRuang;
                        $dtransRuang->idtrans = $find_data->id;
                        $temp = explode("$", $listRuang[$j]);
                        $dtransRuang->idlokasi = $temp[0];
                        $dtransRuang->idunit = $temp[1];
                        $dtransRuang->tanggalmulai = $date_diff_counter;
                        $dtransRuang->tanggalselesai = $date_diff_counter;
                        // $tempTimeMulai = explode(" ",$temptglMulai[$i]);
                        // $tempTimeSelesai = explode(" ",$temptglSelesai[$i]);
                        $dtransRuang->jammulai = $tempJamMulai[$j];
                        $dtransRuang->jamselesai = $tempJamSelesai[$j];
                        $date_diff_counter = date('Y-m-d', (strtotime("+1 day",strtotime($date_diff_counter))));
                        $dtransRuang->save();
                     }
                  }
               }
            // }
         }
      }
      if($listBarang != null){
         for ($i=0; $i < count($listBarang); $i++) {
            $new_dtrans_fasilitas = new DetailBarang;
            $new_dtrans_fasilitas->idtrans = $id;
            $new_dtrans_fasilitas->idbarang = $listBarang[$i];
            $new_dtrans_fasilitas->jumlahbarang = $jumlahBarang[$i];
            $new_dtrans_fasilitas->save();
         }
      }
      $request->session()->flash("success", "Update Transaksi Berhasil ");
      return redirect()->route("list_transaksi");
   }

   public function add_transaksi(Request $r)
   {
      $r->validate([
          'namakegiatan' => 'required|max:255',
          'tanggalpengajuan' => 'required',
          'jumlahpeserta' => 'required|numeric',
          'bentukkegiatan' => 'required|max:255',
          'lampiran' => 'mimes:pdf',
          'namapenanggungjawab' => 'required|max:255',
          'tlppenanggungjawab' => 'required|numeric',
      ]);

      $calendarEvent = new EventCalendar;

      $listRuang = array();
      $listBarang = array();
      $temptglMulai = array();
      $temptglSelesai = array();
      $jumlahBarang = array();

      $calendarEvent->namakegiatan = $r->namakegiatan;
      $calendarEvent->tanggalpengajuan = $r->tanggalpengajuan;
      $calendarEvent->jumlahpeserta = $r->jumlahpeserta;
      $calendarEvent->bentukacara = $r->bentukkegiatan;

      $temptglMulai = $r->tanggalmulai;
      $temptglSelesai = $r->tanggalselesai;
      $tempJamMulai = $r->jammulai;
      $tempJamSelesai = $r->jamselesai;
      $listRuang = $r->data_ruang;
      $listBarang = $r->data_fasilitas;
      $jumlahBarang = $r->jumlahbarang;

      $calendarEvent->keterangankegiatan = $r->keterangan;
      $calendarEvent->penanggungjawab = $r->namapenanggungjawab;
      $calendarEvent->notelp_penanggungjawab = $r->tlppenanggungjawab;
      $calendarEvent->emailpenanggungjawab = $r->emailpenanggungjawab;

      if ($r->file('lampiran')){
        $filename = $r->lampiran->getClientOriginalName();
        $r->lampiran->move('upload/lampiran/', $filename);
        $calendarEvent->lampirankegiatan = $filename;
      }
      $calendarEvent->persetujuan_wr = "PENDING";
      $calendarEvent->persetujuan_baak = "PENDING";
      $calendarEvent->persetujuan_bau = "PENGAJUAN";

      for ($i=0; $i < count($temptglMulai) ; $i++) {
         $temp = explode("&", $listRuang[$i]);
         $idlokasi = $temp[0];
         // dd($idlokasi);
         $idunit = $temp[1];
         // $start_date = date('Y-m-d h:i:s ', strtotime($temptglMulai[$i]));
         // $end_date = date('Y-m-d h:i:s ', strtotime($temptglSelesai[$i]));

         // $start_date_query = DB::table('dtrans_ruang')
         //                         ->where('idlokasi','<>',$idlokasi)
         //                         ->where('tanggalmulai' ,'<>', $start_date)->get();
         // if(count($start_date_query) < 0){
         // $tes= date('Y-m-d',strtotime($temptglMulai[$i]));

         // dd($diff_date->d);
         $daterange_query = DB::select("
                                          select * from dtrans_ruang
                                          where idlokasi = :idlokasi AND substring(tanggalmulai::text,1,10)::date = to_date(:tanggalmulai,'YYYY-MM-DD')
                                          AND (jammulai between :jammulai and :jamselesai OR jamselesai between :jammulai and :jamselesai)
                                       ",
                                       [
                                          'tanggalmulai' => $temptglMulai[$i],
                                          'idlokasi' => $idlokasi,
                                          'jammulai'=> $tempJamMulai[$i],
                                          'jamselesai'   => $tempJamSelesai[$i]
                                       ]);

         // dd(count($daterange_query));
         // dd(count($daterange_query));
            // foreach ($daterange as $key => $value) {
         if(count($daterange_query) > 0){
            $r->session()->flash("errorsBooking", 'Ruang sedang di Gunakan pada tanggal dan jam Tersebut');
            return redirect()->route('add_transaksi')->withInput();
         }
            // }

         // }else{
         //    $r->session()->flash("errorsBooking", 'Ruang Sedang di Gunakan Pada Tanggal Tersebut');
         //    return redirect()->route('add_transaksi')->withInput();
         // }


         // var_dump($tes_daterange);
         // echo "<br>";
         // var_dump($start_date);
         // echo "<br>";
         // var_dump($end_date);
         // echo "<br>";
         // dd($temptglSelesai[$i]);
         // dd($tes_daterange);
         // $temp = explode("&", $listRuang[$i]);
         // $idlokasi = $temp[0];
         // $idunit = $temp[1];
         // $data_ruang = DetailRuang::where('tanggalmulai',$temptglMulai[$i])
         //                ->where('idlokasi',$idlokasi)
         //                ->count();

      }

      //save ke transaksi
      $calendarEvent->save();

      //input tanggal mulai


      //save ke detail transaksi ruang
      for ($j=0; $j < count($temptglMulai) ; $j++) {
         $diff_date = date_diff(date_create($temptglMulai[$j]),date_create($temptglSelesai[$j]));
         $date_diff_counter = $temptglMulai[$j];
         for ($i = 0 ; $i <= $diff_date->d; $i++){
            $dtransRuang = new DetailRuang;
            $dtransRuang->idtrans = $calendarEvent->id;
            $temp = explode("&", $listRuang[$j]);
            $dtransRuang->idlokasi = $temp[0];
            $dtransRuang->idunit = $temp[1];
            $dtransRuang->tanggalmulai = $date_diff_counter;
            $dtransRuang->tanggalselesai = $date_diff_counter;
            // $tempTimeMulai = explode(" ",$temptglMulai[$i]);
            // $tempTimeSelesai = explode(" ",$temptglSelesai[$i]);
            $dtransRuang->jammulai = $tempJamMulai[$j];
            $dtransRuang->jamselesai = $tempJamSelesai[$j];
            $date_diff_counter = date('Y-m-d', (strtotime("+1 day",strtotime($date_diff_counter))));
            $dtransRuang->save();
         }
      }



      for ($i=0; $i < count($listBarang); $i++) {
         $dtransBarang = new DetailBarang;
         $dtransBarang->idtrans = $calendarEvent->id;
         $dtransBarang->namabarang = $listBarang[$i];
         $dtransBarang->jumlahbarang = $jumlahBarang[$i];
         // dd($dtransBarang);
         $dtransBarang->save();
      }


      $r->session()->flash("success", "Transaksi Berhasil di inputkan");
      return redirect()->route("list_transaksi");
   }

   public function delete_dtrans_ruang(Request $r)
   {
      $id = $r->id;
      // dd($id);
      $data = DetailRuang::find($id);
      if($data->delete()){
         echo 'Data berhasil dihapus';
      }
   }

   public function delete_dtrans_barang(Request $r)
   {
      $id = $r->id;
      // dd($id);
      $data = DetailBarang::find($id);
      if($data->delete()){
         echo 'Data berhasil dihapus';
      }
   }

   public function report_room()
   {
      return view('backend.reportroom');
   }
   public function report_countroom()
   {
      return view('backend.reportroom');
   }
   public function report_fasilitas()
   {
      return view('backend.reportfasilitas');
   }


   public function input_transaksi()
   {
      $room = Room::all();
      $fasilitas = Fasilitas::all();

      return view('backend.input_transaksi', [
         'room' => $room ,
         'fasilitas' => $fasilitas ,
      ]);
   }

   public function find_ruang(Request $request)
   {
      $param   = $request->term;
      $data    = Room::where('namalokasi','like', '%'.$param.'%')
                        ->where('idtipelokasi', 'AUDITORIUM')
                        ->OrWhere('idtipelokasi', 'KULIAH')
                        ->orderBy('idtipelokasi', 'asc')
                        ->get();
      $result  = array();
      foreach ($data as $key => $value) {
         $result[] = [ 'idlokasi' => $value->idlokasi , 'namalokasi' => $value->namalokasi ,'idunit' => $value->idunit ];
      }
      return response()->json($result);
   }

   public function find_barang(Request $request)
   {
      $param   = $request->term;
      $data    = Fasilitas::where('namabarang','like', '%'.$param.'%')->get();
      $result  = array();
      foreach ($data as $key => $value) {
         $result[] = [ 'idbarang' => $value->idbarang , 'namabarang' => $value->namabarang  ];
      }
      return response()->json($result);
   }


}
