<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use App\User;
use App\UserRole;

class LoginController extends Controller
{

   public function showLoginPage()
   {
      if(session('login') == 'TRUE'){
         return view("backend.dashboard");
      }else{
         return view('backend.login.pageLogin');
      }
   }

   public function login(Request $request)
   {
      $user_data = DB::connection('pgsql_uwmsdm')->select("select * from sc_user where userid='".$request->username."'");
      // foreach ($user_data as $key => $value) {
      //    echo $value->userid . "<br>";
      //    echo $value->password;
      // }
      // $user_data = User::where('userid', $request->username)->get();
      foreach ($user_data as $key => $value) {
         // dd($value->userid);
         $pass_md5 = md5($request->password);
         if($value->userid == $request->username AND $value->password == $pass_md5){
            // echo " login Berhasil";
            $role = UserRole::where('userid', $value->userid)->get();
            foreach ($role as $key => $value) {
               if($value->idrole == "RT"){
                  $request->session()->put('role', $value->Role->namarole);
                  $request->session()->put('login', 'TRUE');
                  return view('backend.dashboard');
               }
            }
         }
      }


      // if($user_data->userid == $request->username && $user_data->password == $pass_md5){
      //    // inner join antara user, role dan tabel userrole
      //    $role = DB::connection('pgsql_uwmsdm')->select("select sc_role.idrole, sc_role.namarole from sc_userrole
      //                         INNER JOIN sc_role ON sc_userrole.idrole = sc_role.idrole
      //                         where sc_userrole.userid = '" . $request->username ."'");
      //    dd($role);
      //    if(count($role) > 0){
      //       for ($i=0; $i < count($role) ; $i++) {
      //          Session::put('role',$role);
      //          Session::put('username',$request->username);
      //          foreach (Session::get('role') as $key => $value) {
      //
      //          }
      //          return redirect()->route('dashboard');
      //       }
      //    }
      //
      // }
      // $request->session()->flash('login','username dan password tidak valid');
      // return redirect()->route('login');

   }
}
