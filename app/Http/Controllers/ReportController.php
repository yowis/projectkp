<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventCalendar;
use App\DetailRuang;
use App\DetailBarang;

class ReportController extends Controller
{
    //
    public function report_room()
   {
      return view('backend.reportroom');
   }
   public function report_countroom()
   {
      return view('backend.reportroom');
   }
   public function report_fasilitas()
   {
      return view('backend.reportfasilitas');
   }
   public function showReport(Request $request){
      // $potong_tanggal = explode(" - ",$request->daterange);
      // list($start_date, $end_date) = explode(" - ",$request->daterange);
      $start_date = date("Y-m-d", strtotime($request->start_date));
      $end_date = date("Y-m-d", strtotime($request->end_date));
      $data_detail_ruang = DetailRuang::whereBetween('tanggalmulai', [$start_date, $end_date])
                           ->orWhereBetween('tanggalselesai', [$start_date, $end_date])
                           ->get();
                           // dd($data_detail_ruang);
      $data_transaksi = EventCalendar::all();

      // foreach ($data_detail_ruang as $key => $value) {
      //    $data_transaksi = EventCalendar::find($value->idtrans);
      //
      // }
      dd($data_transaksi);
      return view("backend.reportroom",['DetailRuang' => $data_detail_ruang, 'Transaksi' => $data_transaksi]);
      // dd($request->daterange);
   }
}
