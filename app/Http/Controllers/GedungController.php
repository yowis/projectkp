<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gedung;

class GedungController extends Controller
{
    //
    public function list_gedung(){
      $data = Gedung::all();
      // var_dump($data);
      return view('backend.list_gedung', [ 'Gedung' => $data ]);
   }

   public function add_gedung(Request $request)
   {
      $rules = [
        'id' => 'required|unique:in_gedung,idgedung',
      ];

      $customMessages = [
        'required'   => 'The :attribute field can not be blank.',
        'unique'     => ':attribute tidak boleh sama',
      ];

      $this->validate($request, $rules, $customMessages);

      $gedung = new Gedung;
      $gedung->idgedung    = $request->id;
      $gedung->namagedung  = $request->namagedung;
      $gedung->tahunbangun = $request->tahunbangun;
      $gedung->nourut      = $request->nourut;
      $gedung->lokasi      = $request->lokasi;
      $gedung->alamat      = $request->alamat;
      $gedung->jmllantai   = $request->jumlahlantai;

      $gedung->save();
      $request->session()->flash('success','Data Gedung baru berhasil di tambahkan');
      return redirect()->route('list_gedung');
   }

   public function edit_gedung($id)
   {
      $all_data = Gedung::find($id);
      return view('backend.edit_gedung', ['gedung' => $all_data]);
   }

   public function update_gedung(Request $request)
   {

      $gedung = Gedung::find($request->id);
      $gedung->namagedung  = $request->namagedung;
      $gedung->tahunbangun = $request->tahunbangun;
      $gedung->jmllantai   = $request->jumlahlantai;
      $gedung->lokasi      = $request->lokasi;
      $gedung->nourut      = $request->nourut;

      $gedung->save();
      $request->session()->flash('success', 'Gedung berhasil di update');
      return redirect()->route('list_gedung');
   }

   
}
