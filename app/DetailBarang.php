<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailBarang extends Model
{
   //
   protected $connection = 'pgsql';
   protected $table = 'dtrans_barang';

   public $fillable = [
      'idtrans', 'idbarang', 'jumlahbarang'
   ];

   // public function transaksi_ruang()
   // {
   //    return $this->hasOne('App\EventCalendar', 'idtrans', 'id');
   // }
   //
   // public function fasilitas()
   // {
   //    return $this->hasOne('App\Fasilitas', 'idbarang', 'idbarang');
   // }

}
