<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $connection = 'pgsql_uwmsdm';
    protected $table = 'sc_userrole';
    public $timestamps = false;

    public $fillable = [
      'idrole', 'userid', 'satkeridrole', 'isdefault',
   ];

   public function Role()
   {
      return $this->belongsTo('App\Role', 'idrole');
   }

   public function User()
   {
      return $this->belongsTo('App\User', 'userid');
   }
}
