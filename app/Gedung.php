<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Room;

class Gedung extends Model
{
    //
    protected $connection = 'pgsql_uwminv';
   protected $table        = 'in_gedung';
   protected $primaryKey   = 'idgedung';
   protected $keyType      = 'string';
   public $timestamps      = false;

   public $fillable        = [
      'idgedung', 'namagedung', 'tahunbangun', 'nourut', 't_userid', 't_updatetime',
      't_ipaddress', 'jmllantai', 'lokasi', 'alamat', 'stskepemilikan',
   ];


}
