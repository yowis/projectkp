<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
   protected $connection = 'pgsql_uwmsdm';
   protected $table = 'sc_role';
   protected $primaryKey = 'idrole';
   public $timestamps = false;

   public $fillable = [
      'idrole', 'namarole', 'initpage', 'isadmin'
   ];

   public function UserRole()
   {
      return $this->hasMany('App\UserRole', 'idrole', 'idrole');
   }
}
