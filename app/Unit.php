<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
   //
   protected $connection = 'pgsql_uwminv';
   protected $table        = 'ms_unit';
   protected $primaryKey   = 'kodeunit';
   protected $keyType      = 'string';
   public $timestamps   = false;

   protected $fillable     = [
      'kodeunit', 'namaunit', 'namapanjang', 'parentunit', 'level', 'isleaf', 'deflokasi',
      't_userid', 't_updatetime', 't_ipaddress', 'kodeupb', 'nippetugas', 'namapetugas',
      'jabatanpetugas', 'nippetugas2', 'namapetugas2', 'jabatanpetugas2', 'isdipakai',
   ];
   //
}
