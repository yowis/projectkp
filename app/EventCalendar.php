<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCalendar extends Model
{
   //
   protected $connection = 'pgsql';
   protected $table = 'transaksi_ruang';
   public $timestamps      = false;
   public $fillable = [
      'namakegiatan', 'bentukacara', 'jumlahpeserta', 'tanggalpengajuan',
      'keterangankegiatan', 'lampirankegiatan', 'persetujuan_wr', 'persetujuan_bau',
      'persetujuan_baak', 'statuskegiatan', 'penanggungjawab', 'notelp_penanggungjawab',
   ];

   // public function getTanggalMulaiAttribute()
   // {
   //
   //    $id = $this->id;
   //    dd($id);
   //    $data = DetailRuang::find($id)->tanggalmulai;
   //    return $data;
   // }
   public function detailruang()
   {
      return $this->hasMany('App\DetailRuang', 'idtrans', 'id');
   }

   public function DetailBarang()
   {
      return $this->hasMany('App\DetailBarang', 'idtrans', 'id');
   }

}
