<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTransaksiBarang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('dtrans_barang', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idbarang')->nullable();
            $table->string('idtrans')->nullable();
            $table->string('jumlahbarang')->nullable();
            $table->timestamps();
            $table->foreign('idbarang')->references('idbarang')->on('in_barang')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('dtrans_barang');
    }
}
