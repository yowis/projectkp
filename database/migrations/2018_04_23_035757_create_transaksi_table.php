<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_ruang', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('namakegiatan');
            $table->string('bentukacara')->nullable();
            $table->string('jumlahpeserta')->nullable();
            $table->date('tanggalpengajuan');
            $table->text('keterangankegiatan');
            $table->text('lampirankegiatan');
            $table->enum('persetujuan_wr', ['SETUJU', 'TIDAK_SETUJU','PENDING']);
            $table->enum('persetujuan_bau', ['SETUJU', 'TIDAK_SETUJU','PENDING']);
            $table->enum('persetujuan_baak', ['SETUJU', 'TIDAK_SETUJU','PENDING']);
            $table->enum('statuskegiatan', ['SELESAI', 'BERJALAN','PINDAH']);
            $table->string('penanggungjawab', 100)->nullable();;
            $table->string('notelp_penanggungjawab', 20)->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_ruang');
    }
}
