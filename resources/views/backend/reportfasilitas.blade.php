@extends('backend.layouts.app')

@section('add_css')
   <!-- DataTables -->
   <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
   <!-- daterange picker -->
   <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
   <!-- bootstrap datepicker -->
   <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
   <!-- Bootstrap time Picker -->
   <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.min.css') }}">
@endsection

@section('add_js')
   <!-- DataTables -->
   <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
   <!-- bootstrap datepicker -->
   <script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
   <!-- date-range-picker -->
   <script src="{{ asset('bower_components/moment/min/moment.min.js') }}"></script>
   <script src="{{ asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
   <script src="{{ asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
@endsection

@section('add_content')
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1>Report </h1>
     </section>

     <!-- Main content -->
     <section class="content">

     {{-- ERROR HERE   --}}
     @if(count($errors) > 0)
       <div class="callout callout-danger">
       <h4><i class="fa fa-warning"></i> Note:</h4>
       @foreach ($errors->all() as $error)
       {{ $error }} <br />
       @endforeach
       </div>
     @elseif (Session::has('success'))
       <div class="callout callout-success">
         <h4><i class="fa fa-check"></i> Note:</h4>
         {{ Session::get('success') }}
       </div>
     @endif

     <div class="row">
       <div class="col-md-12">
         <div class="box box-primary">
           <div class="box-body">
             <div class="col-md-6">
                <div class="form-group">
                   <label>Date range:</label>
                   <div class="input-group">
                     <div class="input-group-addon">
                       <i class="fa fa-calendar"></i>
                     </div>
                     <input type="text" class="form-control pull-right" id="reservation">
                   </div>
                </div>
                <div class="form-group">
                   <label>Nama Ruang:</label>
                   <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-home"></i>
                    </div>
                    <input type="text" class="form-control">
                  </div>
                </div>
                <!-- /.input group -->

              </div>
           </div>
           <!-- /.box-body -->
           <div class="box-footer">
             <input type="submit" value="Submit" class="btn btn-primary">
           </div>
         </div>
         <!-- /.box -->
       </div>
       <!-- /.col (left) -->
     </div>
     <!-- /.row -->


           <!-- BAR CHART -->
           <div class="box box-success">
            <div class="box-header with-border">
               <h3 class="box-title">Report Fasilitas</h3>

               <div class="box-tools pull-right">
                 <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                 </button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
               </div>
            </div>
            <div class="box-body">
               <table class="table table-bordered" id="tableReportFasilitas">
                  <thead>
                     <tr>
                        <td>Nama Fasilitas</td>
                        <td>Ruang Yang menggunakan</td>
                        <td>jumlah</td>
                        <td>Detail</td>
                     </tr>
                  </thead>
                  <tbody>

                  </tbody>
               </table>
            </div>
            <!-- /.box-body -->
           </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
</div>
@endsection

@push('add_script')
<script>
   $(function (){
      $("#tableReportFasilitas").DataTable()
      //Date range picker
      $('#reservation').daterangepicker()
   })
</script>
@endpush
