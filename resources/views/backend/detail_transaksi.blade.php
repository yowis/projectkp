@extends('backend.layouts.app')

@section('add_content')
<!-- content-wrapper -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Invoice
      <small>#{{ $data_transaksi->id }}</small>
    </h1>

  </section>

  <!-- <div class="pad margin no-print">
    <div class="callout callout-info" style="margin-bottom: 0!important;">
      <h4><i class="fa fa-info"></i> Note:</h4>
      This page has been enhanced for printing. Click the print button at the bottom of the invoice to test.
    </div>
  </div> -->

  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> Detail Transaksi.
          <small class="pull-right">Date: {{ date('d/m/Y ', strtotime($data_transaksi->tanggalpengajuan)) }}</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
      <div class="col-sm-4 invoice-col">
        Penanggung Jawab Acara
        <address>
          <strong>{{ $data_transaksi->penanggungjawab }}</strong><br>
          No. Handphone : {{ $data_transaksi->notelp_penanggungjawab }}<br>
          Email         : {{ $data_transaksi->emailpenanggungjawab }}<br>
        </address>
      </div>
      <!-- /.col -->
      <div class="col-sm-4 invoice-col">
        Nama Kegiatan / Acara
        <address>
          <strong>{{ $data_transaksi->namakegiatan }}</strong><br>
          Bentuk Acara     : {{ $data_transaksi->bentukacara }}<br>
          Jumlah Peserta   : {{ $data_transaksi->jumlahpeserta }} orang<br>
          lampiran Kegiatan: <a href="{{asset('upload/lampiran').'/'.$data_transaksi->lampirankegiatan}}">{{ $data_transaksi->lampirankegiatan }}</a><br>
        </address>
      </div>
      <!-- /.col -->
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <h4>&nbsp;&nbsp;&nbsp;Kebutuhan Ruangan </h4>
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Nama Ruang</th>
            <th>Tanggal Acara Dimulai</th>
            <th>Tanggal Acara Selesai</th>
            <th>Jam Mulai Acara</th>
            <th>Jam Selesai Acara</th>
          </tr>
          </thead>
          <tbody>
             @foreach($data_transaksi->DetailRuang as $key => $value)
               <tr>
                  <td>{{ $value->room->namalokasi }}</td>
                  <td>{{ date('d M Y ', strtotime($value->tanggalmulai)) }}</td>
                  <td>{{ date('d M Y ', strtotime($value->tanggalselesai)) }}</td>
                  <td>{{ $value->jammulai }}</td>
                  <td>{{ $value->jamselesai }}</td>
               </tr>
             @endforeach
          <!-- <tr>
            <td>1</td>
            <td>Call of Duty</td>
            <td>455-981-221</td>
            <td>El snort testosterone trophy driving gloves handsome</td>
            <td>$64.50</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Need for Speed IV</td>
            <td>247-925-726</td>
            <td>Wes Anderson umami biodiesel</td>
            <td>$50.00</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Monsters DVD</td>
            <td>735-845-642</td>
            <td>Terry Richardson helvetica tousled street art master</td>
            <td>$10.70</td>
          </tr>
          <tr>
            <td>1</td>
            <td>Grown Ups Blue Ray</td>
            <td>422-568-642</td>
            <td>Tousled lomo letterpress</td>
            <td>$25.99</td>
          </tr> -->
          </tbody>
        </table>
      </div>
      <!-- /.col -->
      <h4>&nbsp;&nbsp;&nbsp;Kebutuhan Barang / Fasilitas </h4>
      <div class="col-xs-6 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Nama Barang</th>
            <th>Jumlah Barang</th>
          </tr>
          </thead>
          <tbody>
             @foreach($data_transaksi->DetailBarang as $key => $value)
               <tr>
                  <td>{{ $value->namabarang }}</td>
                  <td>{{ $value->jumlahbarang }}</td>
               </tr>
             @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- this row will not appear when printing -->
    <div class="row no-print">
      <div class="col-xs-12">
        <!-- <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a> -->
        <a href="{{ route('list_transaksi') }}"><i class="fa fa-arrow-left"></i> Back Transaksi</a>
      </div>
    </div>
  </section>
  <!-- /.content -->
  <div class="clearfix"></div>
</div>
<!-- /.content-wrapper -->
@endsection
