@extends('backend.login.layouts.app')

@section('content')
<div class="wrap-login100">
   <div class="login100-pic js-tilt" data-tilt>
      <img src="{{ asset('login/images/img-01.png') }}" alt="IMG">
   </div>
   @if(Session::has('login'))
         <span class="alert alert-warning">{{ Session::get('login') }}</span>
   @endif

   <form class="login100-form validate-form" action="{{ route('login') }}" method="post">
      {{ csrf_field() }}
      <span class="login100-form-title">
         Login
      </span>

      <div class="wrap-input100 validate-input">
         <input class="input100" type="text" name="username" placeholder="Username">
         <span class="focus-input100"></span>
         <span class="symbol-input100">
            <i class="fa fa-user" aria-hidden="true"></i>
         </span>
      </div>

      <div class="wrap-input100 validate-input" data-validate = "Password is required">
         <input class="input100" type="password" name="password" placeholder="Password">
         <span class="focus-input100"></span>
         <span class="symbol-input100">
            <i class="fa fa-lock" aria-hidden="true"></i>
         </span>
      </div>

      <div class="container-login100-form-btn">
         <button class="login100-form-btn" type="submit">
            Login
         </button>
      </div>


      <div class="text-center p-t-136">

      </div>
   </form>
</div>
@endsection
