<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset='utf-8'/>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <!-- Tell the browser to be responsive to screen width -->
   <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <link rel="icon" type="image/png" href="{{ asset('bower_components/logo/logo_wm.png') }}" />
   <title>Admin | Managemen Ruang</title>
   {{-- CSS --}}
   @include('backend.login.layouts.linkcss')
   {{-- CSS addon --}}
   @section('add_css')
      @show
</head>
<body>
	<div class="limiter">
		<div class="container-login100">
			@yield('content')
		</div>
	</div>

   {{-- JS HOME --}}
   @include('backend.login.layouts.linkjs')
   {{-- JS ADDON --}}
   @section('add_js')
    @show

   {{-- SCRIPT ADDON --}}
   @stack('add_script')
</body>
</html>
