@extends('backend.layouts.app')

@section('add_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
<!-- Select2 -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css"  />
<!-- Bootstrap Datetime Picker -->
<link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.css') }}" /> -->
<!-- <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.css') }}"> -->
@endsection

@section('add_js')
<!-- daterangepicker -->
<script src="{{ asset('bower_components/moment/min/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- timepicker -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<!-- <script src="{{ asset('bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script> -->
<!-- Custom -->
<script src="{{ asset('backend/dist/js/style.js') }}"></script>
<!-- select2 -->
@endsection

@section('add_content')
   <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
       <h1>Report </h1>
     </section>

     <!-- Main content -->
     <section class="content">

     {{-- ERROR HERE   --}}
     @if(count($errors) > 0)
       <div class="callout callout-danger">
       <h4><i class="fa fa-warning"></i> Note:</h4>
       @foreach ($errors->all() as $error)
       {{ $error }} <br />
       @endforeach
       </div>
     @elseif (Session::has('success'))
       <div class="callout callout-success">
         <h4><i class="fa fa-check"></i> Note:</h4>
         {{ Session::get('success') }}
       </div>
     @endif

     <div class="row">
       <div class="col-md-12">
         <div class="box box-primary">
           <div class="box-body">
             <div class="col-md-6">
                <form class="" action="{{ route('report_ruang') }}" method="post">
                   {{ csrf_field() }}
                <label>Dari Tanggal:</label>


                   <div class="input-group">
                     <div class="input-group-addon">
                       <i class="fa fa-calendar"></i>
                     </div>
                     <input type="text" name="start_date" class="form-control datepicker" id="tgl_mulai" />
                   </div>
                   <!-- /.input group -->


              </div>
              <div class="col-md-6">
                 <label>Sampai Tanggal:</label>
                    <div class="input-group">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" name="end_date" class="form-control datepicker" id="tgl_akhir" />
                    </div>
               </div>
           </div>
           <!-- /.box-body -->
           <div class="box-footer">
             <input type="submit" value="Submit" class="btn btn-success">
             </form>
           </div>
         </div>
         <!-- /.box -->
       </div>
       <!-- /.col (left) -->
     </div>
     <!-- /.row -->


           <!-- BAR CHART -->
           <div class="box box-success">
            <div class="box-header with-border">
               <h3 class="box-title">Report Ruang</h3>

               <div class="box-tools pull-right">
                 <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                 </button>
                 <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
               </div>
            </div>
            <div class="box-body">
               <table class="table table-bordered" id="tableReportRoom">
                  <thead>
                     <tr>
                        <td>Nama Kegiatan</td>
                        <td>Tanggal Acara</td>
                        <td>Detail</td>
                     </tr>
                  </thead>
                  <tbody>
                     @if(count($DetailRuang) > 0)
                        @foreach ($Transaksi as $key => $value)
                           <td>{{ $value->namakegiatan }}</td>
                           <td></td>
                           <td></td>
                           <td></td>
                        @endforeach
                     @endif
                  </tbody>
               </table>
            </div>
            <!-- /.box-body -->
           </div>
           <!-- /.box -->
     </section>
     <!-- /.content -->
</div>
@endsection

@push('add_script')
<script type="text/javascript">
   $(document).ready(function(){
      $(".datepicker").datepicker({
         format : "yyyy-mm-dd"
      });
      $('#tgl_mulai').on('changeDate', function(selected) {
        var startDate = new Date(selected.date.valueOf());
        $("#tgl_akhir").datepicker('setStartDate', startDate);
        if($("#tgl_mulai").val() > $("#tgl_akhir").val()){
          $("#tgl_akhir").val($("#tgl_mulai").val());
        }
      });
   });

</script>
@endpush
