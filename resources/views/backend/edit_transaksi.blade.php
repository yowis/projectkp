@extends('backend.layouts.app')

@section('add_css')
   <!-- DataTables -->
   <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
   <!-- Select2 -->
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
   <!-- Bootstrap Datetime Picker -->
   <link rel="stylesheet" href="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
   <!-- <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.css') }}" /> -->
   <!-- <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.css') }}"> -->

@endsection

@section('add_js')
   <!-- daterangepicker -->
   <script src="{{ asset('bower_components/moment/min/moment.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
   <!-- timepicker -->
   <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
   <!-- <script src="{{ asset('bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script> -->
   <!-- Custom -->
   <script src="{{ asset('backend/dist/js/style.js') }}"></script>
   <!-- select2 -->

@endsection

@section('add_content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Add Event <small>add new event</small></h1>
  </section>

  <!-- Main content -->
  <section class="content">
   {{-- ERROR HERE   --}}
   @if(Session::has('errorsBooking'))
      <div class="callout callout-danger">
      <h4><i class="fa fa-check"></i> Note:</h4>
      {{ Session::get('errorsBooking') }}
     </div>
   @endif
   @if(count($errors) > 0)
     <div class="callout callout-danger">
     <h4><i class="fa fa-warning"></i> Note:</h4>
     @foreach ($errors->all() as $error)
     {{ $error }} <br />
     @endforeach
     </div>
   @elseif (Session::has('success'))
     <div class="callout callout-success">
      <h4><i class="fa fa-check"></i> Note:</h4>
      {{ Session::get('success') }}
     </div>
   @endif
      <form method="post" action="{{ route('edit_transaksi.update') }}" enctype="multipart/form-data">
         {{ csrf_field() }}
         <input name="id" type="hidden" value="{{ $id }}"/>
         <div class="box box-primary">
          <div class="box-header with-border">
             <h3 class="box-title">Data Kegiatan</h3>
             <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
             </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="form-group">
                        <label for="exampleInputEmail1"><span style="color:red;">*</span> Nama Kegiatan</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                           <input type="text" class="form-control" name="namakegiatan" placeholder="Input Nama Kegiatan"  value="{{ $datatrans->namakegiatan }}" />
                        </div>
                     </div>
                  </div>
                 <!-- /Tanggal Pengajuan -->
                 <div class="form-group">
                    <div class="form-group">
                       <label for="exampleInputEmail1"><span style="color:red;">*</span> Tanggal Pengajuan</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input type="text" class="form-control" name="tanggalpengajuan" id="tanggalpengajuan" required placeholder="Input Tanggal Pengajuan" value="{{ $datatrans->tanggalpengajuan }}">
                       </div>
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="form-group">
                       <label for="exampleInputEmail1"><span style="color:red;">*</span> Jumlah Peserta</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-users"></i></span>
                          <input type="text" class="form-control" name="jumlahpeserta"  placeholder="Input Jumlah Peserta" value="{{ $datatrans->jumlahpeserta }}" />
                       </div>
                    </div>
                 </div>

               </div>
               <!-- /.col -->
               <div class="col-md-6">
                  <div class="form-group">
                    <div class="form-group">
                       <label for="exampleInputEmail1"><span style="color:red;">*</span> Bentuk Kegiatan</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                          <input type="text" class="form-control" name="bentukkegiatan" placeholder="Input Bentuk Kegiatan" value="{{ $datatrans->bentukacara }}">
                       </div>
                    </div>
                 </div>
                 <div class="form-group">
                    <label for="exampleInputEmail1">Keterangan Kegiatan</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-sticky-note"></i></span>
                           <textarea class="form-control" name="keterangan" placeholder="Keterangan Kegiatan" >{{ $datatrans->keterangankegiatan }}</textarea>
                        </div>
                 </div>
                 <div class="form-group">
                    <a href="{{asset('upload/lampiran').'/'.$datatrans->lampirankegiatan}}">{{$datatrans->lampirankegiatan}}</a><br />
                    <!-- <embed src="{{asset('upload/lampiran').'/'.$datatrans->lampirankegiatan}}" width="100px" height="100px" /><br /> -->
                   <label for="exampleInputFile"> Lampiran</label>

                   <input type="file" name="lampiran" />
                   <p class="help-block">Surat Pengajuan Kegiatan Kampus .pdf</p>
                 </div>
               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->
          </div>
          <!-- /.box-body -->

         </div>
         <!-- /.box -->

         <div class="row">
           <div class="col-md-12">
             <div class="box box-primary">
               <div class="box-header">
                 <h3 class="box-title">Data Ruang</h3>
               </div>
               <div class="box-body">
                  <table class="table" id="table_ruang">
                     <tr>
                       <td>Nama Ruang</td>
                       <td>Tanggal Mulai</td>
                       <td >Tanggal Selesai</td>
                       <td>Jam Mulai</td>
                       <td>Jam Selesai</td>                       
                       <td><button class="btn btn-sm btn-info" id="btnTambahRuang"><i class="fa fa-plus"></i></button></td>
                    </tr>
                     @if(count($datatrans->DetailRuang) > 0)
                        @foreach($datatrans->DetailRuang as $key => $v)
                        <input type="hidden" name="id_dtransRuang[]" value="{{ $v->id }}"/>
                        <tr id="rowRuang{{ $v->id }}">
                           <td>
                              @if(count($rooms) > 0)
                              <select class="form-control room" style="width:186px;height:20;" name='data_ruang[]' disabled>
                                 @foreach($rooms as $room)
                                    @if($room->idlokasi == $v->idlokasi)
                                       <option value="{{ $room->idlokasi .'$'.$room->idunit }}" selected>{{ $room->namalokasi }}</option>
                                    @else
                                       <option value="{{ $room->idlokasi .'$'.$room->idunit }}" >{{ $room->namalokasi }}</option>
                                    @endif
                                 @endforeach
                              </select>
                              @endif
                           </td>
                           <td><input class="form-control tanggalmulai{{ $v->id }}" type="text" name="tanggalmulai[]" value="{{ $v->tanggalmulai }}" disabled /></td>

<td><input class="form-control tanggalselesai{{ $v->id }}" type="text" name="tanggalselesai[]" value="{{ $v->tanggalselesai }}" disabled/></td>

                           <td>{{$v->jammulai}}</td>
                           <td>{{$v->jamselesai}}</td>
                           <td><a id='{{ $v->id }}' class="btn btn-sm btn-danger linkRemove-dtransRuang"><i class="fa fa-trash"></i></a></td>
                           <!-- <td><button id='{{ $v->id }}' class="btn btn-sm btn-danger btnRemove"><i class="fa fa-trash"></i></button></td> -->
                        </tr>
                        @endforeach
                     @endif
                 </table>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
           <!-- /.col (left) -->

         </div>
         <!-- /.row -->
         <div class="row">
            <div class="col-md-8">
               <div class="box box-primary">
                <div class="box-header with-border">
                   <h3 class="box-title">Data Penanggung Jawab</h3>
                   <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                     <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                   <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <div class="form-group">
                              <label for="exampleInputEmail1"><span style="color:red;">*</span> Nama Penanggung Jawab</label>
                              <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-user-plus"></i></span>
                                 <input type="text" class="form-control" name="namapenanggungjawab" placeholder="Input Nama Penanggung Jawab" value="{{$datatrans->penanggungjawab}}" >
                              </div>
                           </div>
                        </div>
                       <!-- /Tanggal Pengajuan -->
                       <div class="form-group">
                          <div class="form-group">
                             <label for="exampleInputEmail1"><span style="color:red;">*</span> No. Handphone</label>
                             <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
                                <input type="text" class="form-control" name="tlppenanggungjawab"  placeholder="Input Telepon Penanggung Jawab" value="{{$datatrans->notelp_penanggungjawab}}" >
                             </div>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="form-group">
                             <label for="exampleInputEmail1"><span style="color:red;">*</span> Email</label>
                             <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="text" class="form-control" name="emailpenanggungjawab"  placeholder="Input Email Penanggung Jawab" value="{{$datatrans->emailpenanggungjawab}}" >
                             </div>
                          </div>
                       </div>
                     </div>
                     <!-- /.col -->
                   </div>
                   <!-- /.row -->
                </div>
                <!-- /.box-body -->

               </div>
               <!-- /.box -->
            </div>
            <div class="col-md-4">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title">Data Fasilitas</h3>
                </div>
                <div class="box-body">
                  <table class="table table-striped" id="table_barang">
                     <tr>
                      <th>Nama Barang</th>
                      <th>Jumlah</th>
                      <th><button class="btn btn-sm btn-info" id="btnTambahFasilitas"><i class="fa fa-plus"></i></button></th>
                     </tr>

                        @if(count($datatrans->DetailBarang) > 0)
                         @foreach($datatrans->DetailBarang as $key => $value)
                         <input type="hidden" name="id_dtransFasilitas[]" value="{{ $value->id }}"/>
                            <tr id="rowBarang{{ $value->id }}">
                               <td>
                                  @if(count($fasilitas) > 0)
                                  <select class="fasilitas form-control" disabled style="width:186px;height:50px;"  name='data_fasilitas[]'>
                                     @foreach($fasilitas as $key => $f)
                                        @if($f->idbarang == $value->idbarang)
                                           <option value="{{ $f->idbarang }}" selected>{{ $f->namabarang }}</option>
                                        @else
                                           <option value="{{ $f->idbarang }}" >{{ $f->namabarang }}</option>
                                        @endif
                                     @endforeach
                                  </select>
                                  @endif
                               </td>
                               <td><input class="form-control" type="text" name="jumlahbarang[]" value="{{ $value->jumlahbarang }}" disabled></td>
                               <td><button id='{{ $value->id }}' class="btn btn-sm btn-danger linkRemove-dtransBarang"><i class="fa fa-trash btnRemove"></i></button></td>
                            </tr>
                         @endforeach
                        @endif

                 </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
         </div>


         <div class="row">
            <div class="col-md-12">
               <div class="box box-primary">
                  <div class="box-footer">
                     <button class="btn btn-success" type="submit" name="btnSubmit"><i class="fa fa-save"></i> Submit</button>
                  </div>
               </div>
            </div>
         </div>

      </form>
    </section>
    <!-- /.content -->
</div>
@endsection

@push('add_script')
<script>
   $(document).ready(function(){
      $('#tanggalpengajuan').datetimepicker({
         format : 'YYYY-MM-DD'
      });

      @if(count($datatrans->DetailRuang) > 0)
         @foreach($datatrans->DetailRuang as $key => $value)

               $('.tanggalmulai{{ $value->id }}').datetimepicker({
                  format : 'DD-MM-YYYY',
                  date: new Date('{{ $value->tanggalmulai }}'),
                  showClear : true,
               });
               $('.tanggalselesai{{ $value->id }}').datetimepicker({
                  format : 'DD-MM-YYYY',
                  date: new Date('{{ $value->tanggalselesai }}'),
                  showClear : true,
               });

         @endforeach
      @endif


      // $('.tanggalselesai').datetimepicker();

      var rowIdRuang = 1;
      $('#table_ruang').on('click','#btnTambahRuang',function (event){
         event.preventDefault();
         var new_column = '';
         new_column += '<tr id=rowRuang'+rowIdRuang+'>';
         new_column += "<td><select id=ruang"+rowIdRuang+" class='form-control' name='data_ruang[]' style='width:186px;height:50px;'></select></td>";
         new_column += '<td><input id=ruang1'+rowIdRuang+' class="form-control" type="text" name="tanggalmulai[]" placeholder="Tanggal Mulai"/></td>';
         new_column += '<td><input id=ruang2'+rowIdRuang+' class="form-control" type="text" name="tanggalselesai[]" placeholder="Tanggal Selesai"/></td>';
         new_column += '<td><input required id=ruang3'+rowIdRuang+' class="form-control text-center" type="text" name="jammulai[]" placeholder="Jam Mulai"/></td>';
         new_column += '<td><input required id=ruang4'+rowIdRuang+' class="form-control text-center" type="text" name="jamselesai[]" placeholder="Jam Selesai" /></td>';
         new_column += '<td><button id='+rowIdRuang+' class="btn btn-sm btn-danger linkRemove-dtransRuang"><i class="fa fa-trash"></i></button></td></tr>';
         $("#table_ruang").append(new_column);

         $('#ruang'+rowIdRuang).select2({
            ajax: {
               url: '/admin/findruang',
               dataType: 'json',
               delay: 250,
               processResults: function (data) {
                 return {
                   results:  $.map(data, function (item) {
                     return {
                       text: item.namalokasi,
                       id: item.idlokasi + '$' + item.idunit
                     }
                   })
                 };
               },
               cache: true
             }
         });

         $('#ruang1'+rowIdRuang).datetimepicker({
            format : 'YYYY-MM-DD',
            showClear : true,
            useCurrent : true,
         });
         $('#ruang2'+rowIdRuang).datetimepicker({
            format : 'YYYY-MM-DD',
            showClear : true,
         });
         $('#ruang3'+rowIdRuang).datetimepicker({
            format : 'H:mm',
            showClear : true,
         });
         $('#ruang4'+rowIdRuang).datetimepicker({
            format : 'H:mm',
            showClear : true,
         });


         rowIdRuang++;
      });

      $('#table_ruang').on('click', '.linkRemove-dtransRuang', function(event){
         event.preventDefault();
         var button_id = $(this).attr("id");
         if(confirm('Apakah anda yakin menghapus list ruang ini ??')){
            $.ajax({
               type: "GET",
               url: "/admin/deletedtransruang",
               data: {id:button_id},
               cache: false,
               success: function(data){
                  alert(data);
               }
            });

            var rowid = 'rowRuang' + button_id;
            $('#'+rowid+'').remove();
         }else{
            return false;
         }
      });
      // $('#table_ruang').on('click', '.btnRemove', function(event){
      //    event.preventDefault();
      //    var button_id = $(this).attr("id");
      //    // alert(button_id);
      //    if(confirm('Apakah anda yakin menghapus data ini ??')){
      //       var rowid = 'rowRuang' + button_id;
      //       $('#'+rowid+'').remove();
      //    }else{
      //       return false;
      //    }
      // });

      var rowIdBarang = 1;
      $('#table_barang').on('click','#btnTambahFasilitas', function (event){
         event.preventDefault();
         var new_row    = '';
         new_row        += '<tr id=rowBarang'+rowIdBarang+'>';
         new_row        += '<td><select class="listBarang'+rowIdBarang+'"  name="data_fasilitas[]" style="width:186px;height:50px;"><option selected value="">Select Barang</option></select></td>';
         new_row        += '<td><input class="form-control" type="text" name="jumlahbarang[] style="width:186px;height:30px;"></td>';
         new_row        += '<td><button id='+rowIdBarang+' class="btn btn-sm btn-danger linkRemove-dtransBarang"><i class="fa fa-trash btnRemove"></i></button></td></tr>';
         $('#table_barang').append(new_row);
         $('.listBarang'+rowIdBarang).select2({
            ajax: {
               url: '/admin/findbarang',
               dataType: 'json',
               delay: 250,
               processResults: function (data) {
                 return {
                   results:  $.map(data, function (item) {
                     return {
                       text: item.namabarang,
                       id: item.idbarang
                     }
                   })
                 };
               },
               cache: true
             }
         });

         rowIdBarang++;
      });

      $('#table_barang').on('click', '.linkRemove-dtransBarang', function(event){
         event.preventDefault();
         var button_id = $(this).attr("id");
         if(confirm('Apakah anda yakin menghapus list pinjam barang??')){
            var rowid = 'rowBarang' + button_id;
            $.ajax({
               type: "GET",
               url: "/admin/deletedtransbarang",
               data: {id:button_id},
               cache: false,
               success: function(data){
                  alert(data);
               }
            });
            $('#'+rowid+'').remove();
         }else{
            return false;
         }
      });

      $('.room').select2();
      $('.fasilitas').select2();

      // $('#table_barang').on('keyup','.databarang', function(event){
      //    event.preventDefault();
      //    var search = $(this).val();
      // });

      // $('.namabarang').select2({
      //    placeholder: "Select a state",
      //    allowClear: true,
      //    tags: true
      // });

      // $('#add_barang').on('submit',function(event){
      //    event.preventDefault();
      //    console.log('tes 1');
      // });

      // function initializeAutocomplete(rowid){
      //    $('#ruang'+rowid).autocomplete({
      //       source : '/admin/findruang',
      //       minlength : 1,
      //       autofocus : true,
      //    });
      // }

   });

</script>
@endpush
