@extends('backend.layouts.app')

@section('add_css')
   <!-- select2 -->
   <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css') }}">
@endsection

@section('add_js')
<!-- Select2 -->
<script src="{{ asset('bower_components/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
<!-- Slimscroll -->
<script src="{{ asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
@endsection

@section('add_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Add Ruang <small>add new ruang</small></h1>
    </section>

    <!-- Main content -->
    <section class="content">

    {{-- ERROR HERE   --}}
    @if(count($errors) > 0)
      <div class="callout callout-danger">
      <h4><i class="fa fa-warning"></i> Note:</h4>
      @foreach ($errors->all() as $error)
      {{ $error }} <br />
      @endforeach
      </div>
    @elseif (Session::has('error'))
      <div class="callout callout-danger">
        <h4><i class="fa fa-check"></i> Note:</h4>
        {{ Session::get('error') }}
      </div>
      {{ Session::forget('error') }}
    @endif

          <!-- Default box -->
         <div class="box box-info">
              <form role="form" method="post" action="{{ route('add_ruang') }}">
                {{ csrf_field() }}
                <div class="box-body">
                  <div class="row">
                    {{-- LEFT SIDE --}}
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="examplr"><span style="color:red;">*</span> Kode Unit</label>
                           <select class="form-control" id="selectUnit" required name="idunit">
                              <option value="" selected disabled>Select Kode Unit</option>
                              @if (count($unit) > 0)
                                 @foreach ($unit as $key => $value)
                                    <option value="{{ $value->kodeunit }}">{{ $value->namaunit }}</option>
                                 @endforeach
                              @endif
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="examplr"><span style="color:red;">*</span> Kode Ruang</label>
                           <input type="text" class="form-control" name="idlokasi" required placeholder="Kode Ruang">
                        </div>
                        <div class="form-group">
                           <label for="examplr"><span style="color:red;">*</span> Nama Ruang</label>
                           <input type="text" class="form-control" name="namaruang" required placeholder="Nama Ruang">
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Tipe Ruang</label>
                           <select class="form-control" id="selectTipeLokasi" required name="idtipelokasi">
                              <option value="" selected disabled>Select Tipe Ruang</option>
                              @if (count($tipelokasi) > 0)
                                 @foreach ($tipelokasi as $key => $value)
                                    <option value="{{ $value->idtipelokasi }}">{{ $value->keterangan }}</option>
                                 @endforeach
                              @endif
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Gedung</label>
                           <select class="form-control" id="selectGedung" required name="idgedung">
                              <option value="" selected disabled>Select Nama Gedung</option>
                              @if (count($gedung) > 0)
                                 @foreach ($gedung as $key => $value)
                                    <option value="{{ $value->idgedung }}">{{ $value->namagedung }}</option>
                                 @endforeach
                              @endif
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="collects"> Panjang</label>
                           <input type="text" class="form-control" id="e1" name="panjang" placeholder="Panjang">
                        </div>
                        <div class="form-group">
                           <label for="collects"> Lebar</label>
                           <input type="text" class="form-control" id="e1" name="lebar" placeholder="Lebar">
                        </div>
                        <div class="form-group">
                           <label for="collects"> Tinggi</label>
                           <input type="text" class="form-control" id="e1" name="tinggi" placeholder="Tinggi">
                        </div>
                     </div>

                    {{-- RIGHT SIDE --}}
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Lantai</label>
                           <input type="text" class="form-control" id="e1" name="lantai" placeholder="Tinggi">
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Kapasitas</label>
                           <input type="text" class="form-control" id="e1" name="kapasitas" placeholder="Kapasitas Ruangan">
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Luas</label>
                           <input type="text" class="form-control" id="e1" name="luas" placeholder="Luas Ruangan">
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Kondisi</label>
                           <select class="form-control" id="selectKondisi" required name="kondisi">
                              <option value="" selected disabled>Select Kondisi</option>
                              <option value="1">Baik</option>
                              <option value="0">Buruk</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="collects"><span style="color:red;">*</span> Jenjang</label>
                           <select class="form-control" id="selectJenjang" required name="jenjang">
                              <option value="" selected disabled>Select Jenjang Pendidikan</option>
                              <option value="D3">D3</option>
                              <option value="S1">S1</option>
                              <option value="S2">S2</option>
                              <option value="S3">S3</option>
                           </select>
                        </div>
                        <div class="form-group">
                           <label for="collects"> Fungsi Ruang</label>
                           <textarea class="form-control" name="fungsiruang" placeholder="Fungsi Ruang"></textarea>
                        </div>
                        <div class="form-group">
                           <label for="collects"> Interkom</label>
                           <textarea class="form-control" name="interkom" placeholder="ext Interkom"></textarea>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.box-body -->
               <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
               </div>
            </form>
         </div>
         <!-- /.box -->
      </section>
      <!-- /.content -->
   </div>
   <!-- /.content-wrapper -->
@endsection




@push('add_script')
<script>
  $(function () {
    $('#selectJenjang').select2({
      placeholder: "Select Jenjang Pendidikan",
      allowClear: true,
      width: '100%'
    });
    $('#selectKondisi').select2({
      placeholder: "Select Kondisi Gedung",
      allowClear: true,
      width: '100%'
    });
    $('#selectUnit').select2({
      placeholder: "Select Kode Unit",
      allowClear: true,
      width: '100%'
    });
   $('#selectGedung').select2({
      placeholder: "Select Nama Gedung",
      allowClear: true,
      width: '100%'
   });
   $('#selectTipeLokasi').select2({
      placeholder: "Select Tipe Ruang",
      allowClear: true,
      width: '100%'
   });
  });
</script>
@endpush
