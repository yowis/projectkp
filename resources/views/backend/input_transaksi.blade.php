@extends('backend.layouts.app')

@section('add_css')
   <!-- DataTables -->
   <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
   <!-- Select2 -->
   <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
   <!-- Bootstrap Datetime Picker -->
   <link rel="stylesheet" href="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
   <!-- <link rel="stylesheet" href="{{ asset('bower_components/admin-lte/plugins/timepicker/bootstrap-timepicker.css') }}" /> -->
   <!-- <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.css') }}"> -->

@endsection

@section('add_js')
   <!-- daterangepicker -->
   <script src="{{ asset('bower_components/moment/min/moment.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
   <!-- timepicker -->
   <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
   <!-- <script src="{{ asset('bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script> -->
   <!-- Custom -->
   <script src="{{ asset('backend/dist/js/style.js') }}"></script>
   <!-- select2 -->

@endsection

@section('add_content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>Add Event <small>add new event</small></h1>
  </section>

  <!-- Main content -->
  <section class="content">

   {{-- ERROR HERE   --}}
   @if(Session::has('errorsBooking'))
      <div class="callout callout-danger">
      <h4><i class="fa fa-check"></i> Note:</h4>
      {{ Session::get('errorsBooking') }}
     </div>
   @endif
   @if(count($errors) > 0)
     <div class="callout callout-danger">
     <h4><i class="fa fa-warning"></i> Note:</h4>
     @foreach ($errors->all() as $error)
     {{ $error }} <br />
     @endforeach
     </div>
   @elseif (Session::has('success'))
     <div class="callout callout-success">
      <h4><i class="fa fa-check"></i> Note:</h4>
      {{ Session::get('success') }}
     </div>
   @endif
      <form method="post" action="{{ route('add_transaksi') }}" enctype="multipart/form-data">
         {{ csrf_field() }}
         <div class="box box-primary">
          <div class="box-header with-border">
             <h3 class="box-title">Data Kegiatan</h3>
             <div class="box-tools pull-right">
               <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
               <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
             </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
             <div class="row">
               <div class="col-md-6">
                  <div class="form-group">
                     <div class="form-group">
                        <label for="exampleInputEmail1"><span style="color:red;">*</span> Nama Kegiatan</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                           <input type="text" class="form-control" name="namakegiatan" placeholder="Input Nama Kegiatan" value="{{ old('namakegiatan') }}">
                        </div>
                     </div>
                  </div>
                 <!-- /Tanggal Pengajuan -->
                 <div class="form-group">
                    <div class="form-group">
                       <label for="exampleInputEmail1"><span style="color:red;">*</span> Tanggal Pengajuan</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input type="text" class="form-control" name="tanggalpengajuan" id="tanggalpengajuan" required placeholder="Input Tanggal Pengajuan" value="{{ old('tanggalpengajuan') }}">
                       </div>
                    </div>
                 </div>
                 <div class="form-group">
                    <div class="form-group">
                       <label for="exampleInputEmail1"><span style="color:red;">*</span> Jumlah Peserta</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-users"></i></span>
                          <input type="text" class="form-control" name="jumlahpeserta"  placeholder="Input Jumlah Peserta" value="{{ old('jumlahpeserta') }}" />
                       </div>
                    </div>
                 </div>

               </div>
               <!-- /.col -->
               <div class="col-md-6">
                  <div class="form-group">
                    <div class="form-group">
                       <label for="exampleInputEmail1"><span style="color:red;">*</span> Bentuk Kegiatan</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-file-text"></i></span>
                          <input type="text" class="form-control" name="bentukkegiatan" placeholder="Input Bentuk Kegiatan" value="{{ old('bentukkegiatan') }}">
                       </div>
                    </div>
                 </div>
                 <div class="form-group">
                    <label for="exampleInputEmail1">Keterangan Kegiatan</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="fa fa-sticky-note"></i></span>
                           <textarea class="form-control" name="keterangan" placeholder="Keterangan Kegiatan" >{{ old('keterangan') }}</textarea>
                        </div>
                 </div>
                 <div class="form-group">
                   <label for="exampleInputFile"> Lampiran</label>
                   <input type="file" name="lampiran" value="{{ old('lampiran') }}"/>
                   <p class="help-block">Surat Pengajuan Kegiatan Kampus .pdf</p>
                 </div>
               </div>
               <!-- /.col -->
             </div>
             <!-- /.row -->
          </div>
          <!-- /.box-body -->

         </div>
         <!-- /.box -->

         <div class="row">
           <div class="col-md-12">
             <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Data List Ruang Dipinjam</h3>
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                  </div>
               </div>
               <div class="box-body">
                  <table class="table table-striped" id="table_ruang">
                   <tr>
                     <th>Nama Ruang</th>
                     <th>Tanggal Mulai</th>
                     <th>Tanggal Selesai</th>
                     <th>Jam Mulai</th>
                     <th>Jam Selesai</th>
                     <th><button class="btn btn-sm btn-info" id="btnTambahRuang"><i class="fa fa-plus"></i></button></th>
                   </tr>
                 </table>
               </div>
               <!-- /.box-body -->
             </div>
             <!-- /.box -->
           </div>
           <!-- /.col (left) -->

         </div>
         <!-- /.row -->
         <div class="row">
            <div class="col-md-6">
               <div class="box box-primary">
                <div class="box-header with-border">
                   <h3 class="box-title">Data Penanggung Jawab</h3>
                   <div class="box-tools pull-right">
                     <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                     <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                   </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                   <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                           <div class="form-group">
                              <label for="exampleInputEmail1"> Nama Penanggung Jawab</label>
                              <div class="input-group">
                                 <span class="input-group-addon"><i class="fa fa-user-plus"></i></span>
                                 <input type="text" class="form-control" name="namapenanggungjawab" placeholder="Input Nama Penanggung Jawab" value="{{ old('namapenanggungjawab') }}" >
                              </div>
                           </div>
                        </div>
                       <!-- /Tanggal Pengajuan -->
                       <div class="form-group">
                          <div class="form-group">
                             <label for="exampleInputEmail1"> No. Handphone</label>
                             <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-whatsapp"></i></span>
                                <input type="text" class="form-control" name="tlppenanggungjawab"  placeholder="Input Telepon Penanggung Jawab" value="{{ old('tlppenanggungjawab') }}" >
                             </div>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="form-group">
                             <label for="exampleInputEmail1"> Email</label>
                             <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope-o"></i></span>
                                <input type="text" class="form-control" name="emailpenanggungjawab"  placeholder="Input Email Penanggung Jawab" value="{{ old('emailpenanggungjawab') }}" >
                             </div>
                          </div>
                       </div>
                     </div>
                     <!-- /.col -->
                   </div>
                   <!-- /.row -->
                </div>
                <!-- /.box-body -->

               </div>
               <!-- /.box -->
            </div>

            <div class="col-md-6">
              <div class="box box-info">
                 <div class="box-header with-border">
                    <h3 class="box-title">Data Alat dan Fasilitas</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                 </div>
                <div class="box-body">
                  <table class="table table-striped" id="table_barang">
                     <tr>
                      <th >Nama Barang</th>
                      <th >Jumlah </th>
                      <th class="text-center"><button class="btn btn-sm btn-info" id="btnTambahFasilitas"><i class="fa fa-plus"></i></button></th>
                     </tr>
                 </table>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /.box -->
            </div>
            <!-- <div class="col-md-6">
              <div class="box box-info">
                 <div class="box-header with-border">
                    <h3 class="box-title">Data Alat dan Fasilitas</h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
                    </div>
                 </div>
                <div class="box-body">
                  <table class="table table-striped" id="table_data_barang">
                     <tr>
                      <th >Nama Barang</th>
                      <th >Jumlah </th>
                      <th class="text-center"><button class="btn btn-sm btn-info" id="btnTambahdataFasilitas"><i class="fa fa-plus"></i></button></th>
                     </tr>
                 </table>
                </div>
                <!-- /.box-body -->
              <!-- </div> -->
              <!-- /.box -->
            <!-- </div> --> 
         </div>


         <div class="row">
            <div class="col-md-12">
               <div class="box box-primary">
                  <div class="box-footer">
                     <button class="btn btn-success" type="submit" name="btnSubmit"><i class="fa fa-save"></i> Submit</button>
                  </div>
               </div>
            </div>
         </div>

      </form>

    </section>
    <!-- /.content -->
</div>
@endsection

@push('add_script')
<script>
   $(document).ready(function(){
      $('#tanggalpengajuan').datetimepicker({
         format : 'YYYY-MM-DD'
      });

      $('#tanggalmulai').datetimepicker();
      $('#tanggalselesai').datetimepicker();

      var rowIdRuang = 1;
      $('#table_ruang').on('click','#btnTambahRuang',function (event){
         event.preventDefault();
         var new_column = '';
         new_column += '<tr id=rowRuang'+rowIdRuang+'>';
         new_column += "<td><select required id=ruang"+rowIdRuang+" class='form-control' name='data_ruang[]' style='width:178px;height:60px;'></select></td>";
         new_column += '<td><input required id=ruang1'+rowIdRuang+' class="form-control text-center" type="text" name="tanggalmulai[]" /></td>';
         new_column += '<td><input required id=ruang2'+rowIdRuang+' class="form-control text-center" type="text" name="tanggalselesai[]" /></td>';
         new_column += '<td><input required id=ruang3'+rowIdRuang+' class="form-control text-center" type="text" name="jammulai[]" /></td>';
         new_column += '<td><input required id=ruang4'+rowIdRuang+' class="form-control text-center" type="text" name="jamselesai[]" /></td>';
         new_column += '<td><button id='+rowIdRuang+' class="btn btn-sm btn-danger btnRemove"><i class="fa fa-trash"></i></button></td></tr>';
         $("#table_ruang").append(new_column);

         $('#ruang'+rowIdRuang).select2({
            ajax: {
               url: '/admin/findruang',
               dataType: 'json',
               delay: 250,
               processResults: function (data) {
                 return {
                   results:  $.map(data, function (item) {
                     return {
                       text: item.namalokasi,
                       id: item.idlokasi + '&' + item.idunit
                     }
                   })
                 };
               },
               cache: true
             }
         });


         $('#ruang1'+rowIdRuang).datetimepicker({
            format : 'YYYY-MM-DD',
            showClear : true,
         });
         $('#ruang2'+rowIdRuang).datetimepicker({
            format : 'YYYY-MM-DD',
            showClear : true,
         });
         $('#ruang3'+rowIdRuang).datetimepicker({
            format : 'H:mm',
            showClear : true,
         });
         $('#ruang4'+rowIdRuang).datetimepicker({
            format : 'H:mm',
            showClear : true,
         });

         rowIdRuang++;
      });


      $('#table_ruang').on('click', '.btnRemove', function(event){
         event.preventDefault();
         var button_id = $(this).attr("id");
         var rowid = 'rowRuang' + button_id;
         $('#'+rowid+'').remove();
      });

      var rowIdBarang = 1;
      $('#table_barang').on('click','#btnTambahFasilitas', function (event){
         event.preventDefault();
         var new_row    = '';
         new_row        += '<tr id=rowBarang'+rowIdBarang+'>';
         new_row        += '<td><input class="form-control"  type="text" name="data_fasilitas[]" ></td>';
         new_row        += '<td ><input class="form-control"  type="text" name="jumlahbarang[]" ></td>';
         new_row        += '<td><button id='+rowIdBarang+' class="btn btn-sm btn-danger btnRemove"><i class="fa fa-trash btnRemove"></i></button></td></tr>';
         $('#table_barang').append(new_row);
         // $('.listBarang'+rowIdBarang).select2({
         //    ajax: {
         //       url: '/admin/findbarang',
         //       dataType: 'json',
         //       delay: 250,
         //       processResults: function (data) {
         //         return {
         //           results:  $.map(data, function (item) {
         //             return {
         //               text: item.namabarang,
         //               id: item.idbarang
         //             }
         //           })
         //         };
         //       },
         //       cache: true
         //     }
         // });

         rowIdBarang++;
      });

      var rowId_data_Barang = 1;
      $('#table_data_barang').on('click','#btnTambahdataFasilitas', function (event){
         event.preventDefault();
         var new_row    = '';
         new_row        += '<tr id=rowBarang'+rowId_data_Barang+'>';
         new_row        += '<td><input class="form-control"  type="text" name="data_fasilitas[]" ></td>';
         new_row        += '<td ><input class="form-control"  type="text" name="jumlahbarang[]" ></td>';
         new_row        += '<td><button id='+rowId_data_Barang+' class="btn btn-sm btn-danger btnRemove"><i class="fa fa-trash btnRemove"></i></button></td></tr>';
         $('#table_data_barang').append(new_row);
         // $('.listBarang'+rowIdBarang).select2({
         //    ajax: {
         //       url: '/admin/findbarang',
         //       dataType: 'json',
         //       delay: 250,
         //       processResults: function (data) {
         //         return {
         //           results:  $.map(data, function (item) {
         //             return {
         //               text: item.namabarang,
         //               id: item.idbarang
         //             }
         //           })
         //         };
         //       },
         //       cache: true
         //     }
         // });

         rowId_data_Barang++;
      });

      $('#table_barang').on('click', '.btnRemove', function(event){
         event.preventDefault();
         var button_id = $(this).attr("id");
         var rowid = 'rowBarang' + button_id;
         $('#'+rowid+'').remove();
      });

      $('#table_data_barang').on('click', '.btnRemove', function(event){
         event.preventDefault();
         var button_id = $(this).attr("id");
         var rowid = 'rowBarang' + button_id;
         $('#'+rowid+'').remove();
      });

      // $('#table_barang').on('keyup','.databarang', function(event){
      //    event.preventDefault();
      //    var search = $(this).val();
      // });

      // $('.namabarang').select2({
      //    placeholder: "Select a state",
      //    allowClear: true,
      //    tags: true
      // });

      // $('#add_barang').on('submit',function(event){
      //    event.preventDefault();
      //    console.log('tes 1');
      // });

      // function initializeAutocomplete(rowid){
      //    $('#ruang'+rowid).autocomplete({
      //       source : '/admin/findruang',
      //       minlength : 1,
      //       autofocus : true,
      //    });
      // }

   });

</script>
@endpush
