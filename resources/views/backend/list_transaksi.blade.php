@extends('backend.layouts.app')

@section('add_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('add_js')
   <!-- DataTables -->
   <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
@endsection

@section('add_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>List Transaksi Ruang</h1>
    </section>

    <!-- Main content -->
    <section class="content">

    {{-- ERROR HERE   --}}
    @if(count($errors) > 0)
      <div class="callout callout-danger">
      <h4><i class="fa fa-warning"></i> Note:</h4>
      @foreach ($errors->all() as $error)
      {{ $error }} <br />
      @endforeach
      </div>
    @elseif (Session::has('success'))
      <div class="callout callout-success">
        <h4><i class="fa fa-check"></i> Note:</h4>
        {{ Session::get('success') }}
      </div>
    @endif

          <!-- Default box -->
          <div class="box box-success">
            <div class="box-header">
               <h3 class="box-title">Transaksi Success</h3>
            </div>
            <div class="box-body" style="overflow-x: scroll;">
              <table id="tableTansaksiSuccess" class="table table-bordered">
               <thead>
                   <tr>
                     <th>ID</th>
                     <th>Nama Event</th>
                     <th>Tanggal Pengajuan</th>
                     <th>Persetujuan BAAK</th>
                     <th>Persetujuan W. Rektor</th>
                     <th>Persetujuan BAU</th>
                     <th>Detail</th>
                     <th class="text-center no-sort">Action</th>
                   </tr>
               </thead>
                  <tbody>
                      @if(count($transaksi) > 0)
                        @foreach ($transaksi as $key => $value)
                           <tr>
                              <th>{{ $value->id }}</th>
                              <td>{{ $value->namakegiatan }}</td>
                              <td>{{ date('d M Y ', strtotime($value->tanggalpengajuan)) }}</td>
                              <td class="text-center no-sort">
                                 @if($value->persetujuan_baak == 'PENDING')
                                    <span class="label label-warning"><i class="fa fa-hourglass-start"></i>  &nbsp;{{ $value->persetujuan_baak }}</span>
                                 @else
                                    <span class="label label-success"><i class="fa fa-check-square-o"></i>  &nbsp;{{ $value->persetujuan_baak }}</span>
                                 @endif
                              </td>
                              <td class="text-center no-sort">
                                 @if($value->persetujuan_wr == 'PENDING')
                                    <span class="label label-warning"><i class="fa fa-hourglass-start"></i>  &nbsp;{{ $value->persetujuan_wr }}</span>
                                 @else
                                    <span class="label label-success"><i class="fa fa-check-square-o"></i>  &nbsp;{{ $value->persetujuan_wr }}</span>
                                 @endif
                              </td>
                              <td class="text-center no-sort">
                                 @if($value->persetujuan_bau == 'PENDING')
                                    <span class="label label-warning"><i class="fa fa-hourglass-start"></i>  &nbsp;{{ $value->persetujuan_bau }}</span>
                                 @else
                                    <span class="label label-success"><i class="fa fa-check-square-o"></i>  &nbsp;{{ $value->persetujuan_bau }}</span>
                                 @endif

                              </td>
                              <td><a href="{{ route('detail_transaksi',['id' => $value->id]) }}" class="btn bg-navy btn-flat btn-sm"><i class="fa fa-eye"></i> Detail</a></td>
                              <td>
                                 <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-flat"><i class="fa fa-wrench"></i></button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                      <span class="caret"></span>
                                      <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                       <li><a href="{{ route('edit_transaksi', ['id' => $value->id ] ) }}">Edit</a></li>
                                       <li><a href="{{ route('delete_transaksi', ['id' => $value->id ] ) }}">Delete</a></li>
                                       <li><a href="{{ route('approve_transaksi', ['id' => $value->id ] ) }}">Approve</a></li>
                                    </ul>
                                  </div>
                              </td>
                           </tr>
                        @endforeach
                      @endif
                  </tbody>
               </table>
            </div>
          </div>
          <!-- /.box -->
          <!-- <div class="box box-warning">
             <div class="box-header">
              <h3 class="box-title">Transaksi Waiting</h3>
            </div>
            <div class="box-body" style="overflow-x: scroll;">
              <table id="tableTansaksiOther" class="table table-bordered">
                <thead>
                <tr>
                  <th>Nama Event</th>
                  <th>Tanggal Mulai</th>
                  <th>Tanggal Selesai</th>
                  <th>Persetujuan</th>
                  <th>Detail</th>
                  <th class="text-center no-sort">Action</th>
                </tr>
                </thead>
                <tbody>

                   <tr>
                     <td>Event A</td>
                     <td>11 Mei 2017</td>
                     <td>15 Mei 2017</td>
                     <td><span class="label label-warning"><i class="fa fa-hourglass-start"></i> PENDING</span></td>
                     <td><a href="#" class="btn bg-navy btn-flat btn-sm"><i class="fa fa-eye"></i> Detail</a></td>
                     <td class="text-center">
                        <div class="btn-group">
                           <button type="button" class="btn btn-default btn-flat"><i class="fa fa-wrench"></i></button>
                           <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                             <span class="caret"></span>
                             <span class="sr-only">Toggle Dropdown</span>
                           </button>
                           <ul class="dropdown-menu" role="menu">
                             <li><a href="">Edit</a></li>
                             <li><a href="#">Delete</a></li>
                             <li><a href="#">Approve</a></li>
                           </ul>
                         </div>
                     </td>
                  </tr>
                  <tr>
                     <td>Event A</td>
                     <td>11 Mei 2017</td>
                     <td>15 Mei 2017</td>
                     <td><span class="label label-warning"><i class="fa fa-hourglass-start"></i> PENDING</span></td>
                     <td><a href="#" class="btn bg-navy btn-flat btn-sm"><i class="fa fa-eye"></i> Detail</a></td>
                     <td class="text-center">
                        <div class="btn-group">
                           <button type="button" class="btn btn-default btn-flat"><i class="fa fa-wrench"></i></button>
                           <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                             <span class="caret"></span>
                             <span class="sr-only">Toggle Dropdown</span>
                           </button>
                           <ul class="dropdown-menu" role="menu">
                             <li><a href="#">Edit</a></li>
                             <li><a href="#">Delete</a></li>
                             <li><a href="#">Approve</a></li>
                           </ul>
                         </div>
                     </td>
                  </tr>
                  <tr>
                     <td>Event A</td>
                     <td>11 Mei 2017</td>
                     <td>15 Mei 2017</td>
                     <td><span class="label label-warning"><i class="fa fa-hourglass-start"></i> PENDING</span></td>
                     <td><a href="#" class="btn bg-navy btn-flat btn-sm"><i class="fa fa-eye"></i> Detail</a></td>
                     <td class="text-center">
                        <div class="btn-group">
                           <button type="button" class="btn btn-default btn-flat"><i class="fa fa-wrench"></i></button>
                           <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                             <span class="caret"></span>
                             <span class="sr-only">Toggle Dropdown</span>
                           </button>
                           <ul class="dropdown-menu" role="menu">
                             <li><a href="#">Edit</a></li>
                             <li><a href="#">Delete</a></li>
                             <li><a href="#">Approve</a></li>
                           </ul>
                         </div>
                     </td>
                  </tr>
                  <tr>
                     <td>Event A</td>
                     <td>11 Mei 2017</td>
                     <td>15 Mei 2017</td>
                     <td><span class="label label-warning"><i class="fa fa-hourglass-start"></i> PENDING</span></td>
                     <td><a href="#" class="btn bg-navy btn-flat btn-sm"><i class="fa fa-eye"></i> Detail</a></td>
                     <td class="text-center">
                        <div class="btn-group">
                           <button type="button" class="btn btn-default btn-flat"><i class="fa fa-wrench"></i></button>
                           <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                             <span class="caret"></span>
                             <span class="sr-only">Toggle Dropdown</span>
                           </button>
                           <ul class="dropdown-menu" role="menu">
                             <li><a href="#">Edit</a></li>
                             <li><a href="#">Delete</a></li>
                             <li><a href="#">Approve</a></li>
                           </ul>
                         </div>
                     </td>
                  </tr>
                  <tr>
                     <td>Event A</td>
                     <td>11 Mei 2017</td>
                     <td>15 Mei 2017</td>
                     <td><span class="label label-warning"><i class="fa fa-hourglass-start"></i> PENDING</span></td>
                     <td><a href="#" class="btn bg-navy btn-flat btn-sm"><i class="fa fa-eye"></i> Detail</a></td>
                     <td class="text-center">
                        <div class="btn-group">
                           <button type="button" class="btn btn-default btn-flat"><i class="fa fa-wrench"></i></button>
                           <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                             <span class="caret"></span>
                             <span class="sr-only">Toggle Dropdown</span>
                           </button>
                           <ul class="dropdown-menu" role="menu">
                             <li><a href="#">Edit</a></li>
                             <li><a href="#">Delete</a></li>
                             <li><a href="#">Approve</a></li>
                           </ul>
                         </div>
                     </td>
                  </tr>
                </tbody>
              </table>
            </div>

          </div> -->
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@push('add_script')
<script>
  $(function () {
    $("#tableTansaksiSuccess").DataTable();
    $("#tableTansaksiOther").DataTable();
   //  $('#tableRoom').DataTable({
   //    'paging'      : true,
   //    'lengthChange': true,
   //    'searching'   : true,
   //    'ordering'    : true,
   //    'info'        : false,
   //    'autoWidth'   : true
   // });
  });
</script>
@endpush
