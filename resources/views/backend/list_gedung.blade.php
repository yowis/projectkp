@extends('backend.layouts.app')

@section('add_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('add_js')
   <!-- DataTables -->
   <script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
   <script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
@endsection

@section('add_content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>List Gedung</h1>
    </section>

    <!-- Main content -->
    <section class="content">

    {{-- ERROR HERE   --}}
    @if(count($errors) > 0)
      <div class="callout callout-danger">
      <h4><i class="fa fa-warning"></i> Note:</h4>
      @foreach ($errors->all() as $error)
      {{ $error }} <br />
      @endforeach
      </div>
    @elseif (Session::has('success'))
      <div class="callout callout-success">
        <h4><i class="fa fa-check"></i> Note:</h4>
        {{ Session::get('success') }}
      </div>
    @endif

          <!-- Default box -->
          <div class="box box-info">
            <div class="box-body" style="overflow-x: scroll;">
              <table id="tableGedung" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ID Gedung</th>
                  <th>Nama</th>
                  <th>Tahun Bangun</th>
                  <th>Jumlah Lantai</th>
                  <th>Lokasi</th>
                  <th>Alamat</th>
                  <th>Status Kepemilikan</th>
                  <th>No. Urut</th>
                  <th class="text-center no-sort">Action</th>
                </tr>
                </thead>
                <tbody>
                   <!-- {{ var_dump($Gedung)}} -->
                @if(count($Gedung) > 0)
                  @foreach ($Gedung as $key => $value)
                    <tr>

                      <td>{{ $value->idgedung }}</td>
                      <td>{{ $value->namagedung }}</td>
                      <td>{{ $value->tahunbangun }}</td>
                      <td>{{ $value->jmllantai }}</td>
                      <td>{{ $value->lokasi }}</td>
                      <td>{{ $value->alamat }}</td>
                      <td>{{ $value->stskepemilikan }}</td>
                      <td>{{ $value->nourut }}</td>
                      <td class="text-center">
                        <div class="show-inline-flex">
                           <a class="btn btn-xs" style="background-color: #337ab7; color: #fff;" href="{{ route('edit_gedung', ['id' => $value->idgedung ]) }}"><i class="fa fa-pencil"></i> Edit</a>
                           <form id="form-delete" action="{{ route('list_gedung') }}" method="post">
                              <input type="hidden" name="id" value="{{ $value->idgedung }}">
                              {{ csrf_field() }}
                              <button class="btn btn-xs bg-navy">
                                <i class="fa fa-trash"></i> Delete
                              </button>
                           </form>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                @endif
                </tbody>
              </table>
            </div>

          </div>
          <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection

@push('add_script')
<script>
  $(function () {
   //  $("#tableGedung").DataTable();
   // //  $('#tableRoom').DataTable({
   // //    'paging'      : true,
   // //    'lengthChange': true,
   // //    'searching'   : true,
   // //    'ordering'    : true,
   // //    'info'        : false,
   // //    'autoWidth'   : true
   // // });
   $('#tableGedung').DataTable( {
        "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]]
    } );
  });
</script>
@endpush
