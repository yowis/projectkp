<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel" style="min-height: 65px;">
      <div class="pull-left image">
        <img src="{{ asset('backend/admin_icon.jpg') }}" class="img-circle">
      </div>
      <div class="pull-left info">
        <p>Admin</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        <!-- {{-- <p>{{ Auth::user()->real_name }}</p> --}} -->
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->

    {{-- DASHBOARD --}}
    <ul class="sidebar-menu tree" data-widget="tree">
      {{-- role --}}
      <li class="header">HAK AKSES</li>
      <li class="treeview ">
        <a href="#">
          <i class="fa fa-map"></i>
          <span>Hak Akses</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="">
            <a href=""><i class="fa fa-circle-o"></i> {{Session::get('role')}}</a>
          </li>
        </ul>
      </li>


      <li class="header">MENU NAVIGATION</li>

      {{-- DASHBOARD --}}
      <li class="{{ Route::currentRouteNamed('dashboard') ? 'active' : null }}">
        <a href={{ route('dashboard') }}>
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
        </a>
      </li>

      {{-- CALENDAR --}}
      <li class="{{ Route::currentRouteNamed('calendar') ? 'active' : null }}">
        <a href={{ route('calendar') }}>
          <i class="fa fa-calendar"></i>
          <span>Calendar</span>
        </a>
      </li>


      {{-- RUANG --}}
      @if(Session::get('role') == "pegawai")
      <li class="treeview {{ Route::currentRouteNamed('add_ruang')||Route::currentRouteNamed('edit_ruang')||Route::currentRouteNamed('list_ruang') ? 'active' : null }}">
        <a href="#">
          <i class="fa fa-map"></i>
          <span>Ruang</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Route::currentRouteNamed('add_ruang') ? 'active' : null }}">
            <a href="{{ route('add_ruang') }}"><i class="fa fa-plus"></i> Tambah Ruang</a>
          </li>
          <li class="{{ Route::currentRouteNamed('list_ruang')||Route::currentRouteNamed('edit_ruang') ? 'active' : null }}">
            <a href="{{ route('list_ruang') }}"><i class="fa fa-list-alt"></i> List Ruang</a>
          </li>
        </ul>
      </li>

      {{-- FASILITAS --}}
      <li class="treeview {{ Route::currentRouteNamed('add_fasilitas')||Route::currentRouteNamed('edit_fasilitas')||Route::currentRouteNamed('list_fasilitas') ? 'active' : null }}">
        <a href="#">
          <i class="fa fa-wrench"></i>
          <span>Fasilitas</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Route::currentRouteNamed('add_fasilitas') ? 'active' : null }}">
            <a href="{{ route('add_fasilitas') }}"><i class="fa fa-plus"></i> Tambah Fasilitas</a>
          </li>
          <li class="{{ Route::currentRouteNamed('list_fasilitas')||Route::currentRouteNamed('edit_fasilitas') ? 'active' : null }}">
            <a href="{{ route('list_fasilitas') }}"><i class="fa fa-list-alt"></i> List Fasilitas</a>
          </li>
        </ul>
      </li>

      {{-- Gedung --}}
      <li class="treeview {{ Route::currentRouteNamed('add_gedung')||Route::currentRouteNamed('edit_gedung')||Route::currentRouteNamed('list_gedung') ? 'active' : null }}">
        <a href="#">
          <i class="fa fa-building"></i>
          <span>Gedung</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Route::currentRouteNamed('add_gedung') ? 'active' : null }}">
            <a href="{{ route('add_gedung') }}"><i class="fa fa-plus"></i> Tambah Gedung</a>
          </li>
          <li class="{{ Route::currentRouteNamed('list_gedung')||Route::currentRouteNamed('edit_gedung') ? 'active' : null }}">
            <a href="{{ route('list_gedung') }}"><i class="fa fa-list-alt"></i> List Gedung</a>
          </li>
        </ul>
      </li>
      @endif
      {{-- TRANSAKSI --}}
      <li class="treeview {{ Route::currentRouteNamed('add_transaksi')||Route::currentRouteNamed('edit_transaksi')||Route::currentRouteNamed('list_transaksi')||Route::currentRouteNamed('detail_transaksi') ? 'active' : null }}">
        <a href="#">
          <i class="fa fa-tasks"></i>
          <span>Transaksi Peminjaman</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Route::currentRouteNamed('add_transaksi') ? 'active' : null }}">
            <a href="{{ route('add_transaksi') }}"><i class="fa fa-plus"></i> Tambah Transaksi</a>
          </li>
          <li class="{{ Route::currentRouteNamed('list_transaksi')||Route::currentRouteNamed('edit_transaksi')||Route::currentRouteNamed('detail_transaksi') ? 'active' : null }}">
            <a href="{{ route('list_transaksi') }}"><i class="fa fa-list-alt"></i> List Transaksi</a>
          </li>
        </ul>
      </li>

      {{-- REPORT --}}
      @if(Session::get('role') == "Rumah Tangga")
      <li class="treeview {{ Route::currentRouteNamed('report_ruang')||Route::currentRouteNamed('report_fasilitas')||Route::currentRouteNamed('report_countroom') ? 'active' : null }}">
        <a href="#">
          <i class="fa fa-file-text-o"></i>
          <span>Report</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="{{ Route::currentRouteNamed('report_ruang') ? 'active' : null }}">
            <a href="{{ route('report_ruang') }}"><i class="fa fa-line-chart"></i> Report Ruang</a>
          </li>
          <li class="{{ Route::currentRouteNamed('report_fasilitas') ? 'active' : null }}">
            <a href="{{ route('report_fasilitas') }}"><i class="fa fa-line-chart"></i> Report Fasilitas Ruang</a>
          </li>
        </ul>
      </li>
      @endif

    </ul>
  </section>
  <!-- /.sidebar --> -->
</aside>
