<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset='utf-8'/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('bower_components/logo/logo_wm.png') }}" />
    <title>Admin | Managemen Ruang</title>

    {{-- CSS --}}
    @include('backend.layouts.linkcss')
    {{-- CSS addon --}}
    @section('add_css')
        @show

  </head>
    <body class="skin-blue sidebar-mini">
    <!-- Preloader -->
    <!-- <div id="preloader-div" class="preloader-show">
      <div class="container-preloader">
        <svg class="preloader" xmlns="http://www.w3.org/2000/svg" version="1.1" width="600" height="200">
          <defs>
            <filter id="goo" x="-40%" y="-40%" height="200%" width="400%">
              <feGaussianBlur in="SourceGraphic" stdDeviation="10" result="blur" />
              <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -8" result="goo" />
            </filter>
          </defs>
          <g filter="url(#goo)">
            <circle class="dot" cx="50" cy="50" r="25" fill="#8d81ac" />
            <circle class="dot" cx="50" cy="50" r="25" fill="#8d81ac" />
          </g>
        </svg>
        <div class="loadingText">Loading...</div>
      </div>
    </div> -->


    <div class="wrapper">
      {{-- HEADER --}}
      @include('backend.layouts.header')
      {{-- SIDEBAR --}}
      @include('backend.layouts.sidebar')

      {{-- CONTENT --}}
      @section('add_content')

      @show

      {{-- CONTENT --}}
      @include('backend.layouts.footer')
    </div>

      {{-- JS HOME --}}
      @include('backend.layouts.linkjs')
      {{-- JS ADDON --}}
      @section('add_js')
        @show

      {{-- SCRIPT ADDON --}}
      @stack('add_script')

  </body>
</html>
