<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/', 'HomeController@index');
//Route::get('/',function(){ return view('welcome'); });
Route::get('/', 'LoginController@showLoginPage')->name('login');
Route::post('/', 'LoginController@login');

Route::prefix('admin')->group(function(){
   //AUTH MANUAL
   // Route::get('/login', 'LoginController@showLoginPage')->name('login');
   // Route::post('/login', 'LoginController@login');

   Route::get('/dashboard', 'CalendarController@showDashboard')->name('dashboard');
   Route::get('/', 'CalendarController@showDashboard')->name('dashboard');
   Route::get('/calendar', 'CalendarController@index')->name('calendar');

   Route::get('/addaksesuser',function(){ return view('backend.input_akses_user'); })->name('add_akses_user');
   Route::post('/addaksesuser','UserController@add_akses_user');
   Route::get('/listaksesuser','UserController@list_akses_user');
   Route::get('/editaksesuser/{id}','UserController@edit_akses_user')->name('edit_akses_user');
   Route::post('/editaksesuser/{id}','UserController@update_akses_user');
   
   Route::get('/addfasilitas', function(){ return view('backend.input_fasilitas'); })->name('add_fasilitas');
   Route::post('/addfasilitas','FasilitasController@add_Fasilitas');
   Route::get('/listfasilitas','FasilitasController@list_Fasilitas')->name('list_fasilitas');
   Route::get('/editfasilitas/{id}','FasilitasController@edit_fasilitas')->name('edit_fasilitas');
   Route::post('/editfasilitas/{id}','FasilitasController@update_fasilitas');

   Route::get('/addruang', 'RuangController@input_ruang')->name('add_ruang');
   Route::post('/addruang', 'RuangController@add_ruang');
   Route::get('/listruang','RuangController@list_Ruang')->name('list_ruang');
   Route::post('/editruang','RuangController@edit_ruang')->name('edit_ruang');
   Route::post('/editruang/update','RuangController@update_ruang')->name('edit_ruang.update');
   Route::get('/detailruang/{id}','RuangController@detail_ruang')->name('detail_ruang');

   Route::get('/addgedung', function(){ return view('backend.input_gedung'); })->name('add_gedung');
   Route::post('/addgedung', 'GedungController@add_gedung');
   Route::get('/listgedung','GedungController@list_gedung')->name('list_gedung');
   Route::get('/editgedung/{id}','GedungController@edit_gedung')->name('edit_gedung');
   Route::post('/editgedung/{id}','GedungController@update_gedung');

   Route::get('/addtransaksi', 'TransaksiController@input_transaksi')->name('add_transaksi');
   Route::post('/addtransaksi', 'TransaksiController@add_transaksi');
   Route::get('/listtransaksi','TransaksiController@list_transaksi')->name('list_transaksi');
   Route::get('/edittransaksi/{id}','TransaksiController@edit_transaksi')->name('edit_transaksi');
   Route::post('/edittransaksi/update','TransaksiController@update_transaksi_ruang')->name('edit_transaksi.update');
   Route::post('/deletetransaksi/{id}','TransaksiController@delete_transaksi')->name('delete_transaksi');
   Route::get('/approveransaksi/{id}','TransaksiController@approve_transaksi')->name('approve_transaksi');
   Route::get('/detailtransaksi/{id}','TransaksiController@Detail_transaksi')->name('detail_transaksi');

   Route::get('/findruang','TransaksiController@find_ruang');
   Route::get('/deletedtransruang', 'TransaksiController@delete_dtrans_ruang' );
   Route::get('/deletedtransbarang', 'TransaksiController@delete_dtrans_barang' );

   Route::get('/reportruang', 'ReportController@report_room')->name('report_ruang');
   Route::get('/reportfasilitas', 'ReportController@report_fasilitas')->name('report_fasilitas');
   Route::get('/reportcountroom', 'ReportController@report_countroom')->name('report_countroom');
   Route::post('/reportruang', 'ReportController@showReport');

   Route::get('/findruang','TransaksiController@find_ruang')->name('find_ruang');
   Route::get('/findbarang','TransaksiController@find_barang')->name('find_barang');
});
